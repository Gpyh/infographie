/**
 * Utilisation de plusieurs textures pour une facette.
 * Pour cela on utilise plusieurs unités de texture.
 * Ici, l'unité de texture GL_TEXTURE0 contient la shadow map et
 * l'unité de texture GL_TEXTURE1 contient la texture qui est plaquée sur le sol.
 * Commandes OpenGL utilisées pour gérer les unités de textures :
 * - glActiveTexture(GL_TEXTUREi);
 * - glMultiTexCoord2d(GL_TEXTUREi, s, t);
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include <SDL.h>
#include <SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifndef M_PI
#define M_PI 3.141592654
#endif

#define degRad(a) (((double) a) * M_PI / 180.)

// Taille de la shadow map
// Attention les versions un peu vieilles d'OpengL
// ne supportent que des tailles qui sont des puissances de 2
// On ne peut pas avoir un viewport plus grand que la fenêtre
#define SHADOW_MAP_WIDTH 512
#define SHADOW_MAP_HEIGHT 512

// Volume de projection utilisé lorsqu'on calcule la shadow map
float lightFovy = 90.;
float lightAspect = 1.;
float lightNearPlane = 2.5;
float lightFarPlane = 5;

// Id de la depth texture contenant la shadow map
static GLuint shadowMap;

/* Position de l'observateur */
float rho = 15;
float theta = 90; // angle (Ox, OP') (P' est le projeté de P dans le plan Oxz)
float phi = 30;   // angle (OP', OP)

/* Gestion de la souris */
int boutonGaucheAppuye = false;

/* Lumière (fixe par rapport à la scène) */
GLfloat lumPosition[4] = {4., 2., .5, 1.};
GLfloat lumUp[4] = {-1., 1, 0., 1.};

// Propriétés de la source lumineuse
GLfloat lumiere[4] = {1., 1., 1., 1.};
GLfloat lumiereAmbiante[4] = {.4, .4, .4, 1.};
GLfloat lumZero[4] = {0., 0., 0., 1.};

// Propriétés des matériaux
GLfloat matGris[4] = {.4, .4, .4, 1.};
GLfloat matRouge[4] = {.2, .0, .0, 1.};
GLfloat matVert[4] = {.0, .2, .0, 1.};
GLfloat matBleu[4] = {.0, .0, .2, 1.};
GLfloat matSpeculaire[4] = {1., 1., 1., 1.};
GLfloat matShininess = 50.;

/* Une quadrique pour la GLU */
GLUquadric * quad;

int drawShadows = 0;

SDL_Window *fenetre;
SDL_GLContext context;

GLenum erreur;

void dessineScene();
void dessineSol();
void positionneObservateur();

/**
 * Une fonction qui transpose une matrice.
 */
void transposeMatrix(GLdouble m[16]) {
  int i,j;
  GLdouble t;

  for (i = 0; i < 4; i++) {
    for (j = i + 1; j < 4; j++) {
      t = m[i * 4 + j];
      m[i * 4 + j] = m[j * 4 + i];
      m[j * 4 + i] = t;
    }
  }
}

/**
 * Initialisation de la texture qui sera plaquée sur le sol.
 * Cette texture sera stockée dans l'unité de texture 1.
 */
void initTexture() {
  // On active l'unité de texture 1
  // Toutes les commandes portant sur les textures affecteront alors
  // cette unité.
  glActiveTexture(GL_TEXTURE1);

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  SDL_Surface *image;

  image = IMG_Load("marblewhite.bmp");
  if(!image) {
    fprintf(stderr, "Impossible de charger l'image : %s\n", IMG_GetError());
    exit(EXIT_FAILURE);
  }

  if (image->format->BytesPerPixel == 3) {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
  } else {
    fprintf(stderr, "Nombre de composants de l'image différent de 3 (%d)\n", image->format->BytesPerPixel);
    exit(EXIT_FAILURE);
  }
  
  SDL_FreeSurface(image);

  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  // On replace l'unité de texture 0 comme unité active
  glActiveTexture(GL_TEXTURE0);
  
  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans initTexture : %s\n", gluErrorString(erreur));
  }
}

GLdouble shadowMapProjectionMatrix[16];

/**
 * Création de la shadow map.
 * On crée une texture dont les valeurs correspondent - grosso-modo - à la
 * distance entre la lumiere et les points éclairés.
 * Cette shadow map est crée dans l'unité de texture courante (on ne fait pas
 * de glActiveTexture).
 * Ici, ce sera l'unité 0.
 * De même la texture est stockée dans l'objet texture courant
 * (on ne fait pas de glBindTexture).
 */
void creeShadowMap() {
  GLint viewport[4];

  glGetIntegerv(GL_VIEWPORT, viewport);

  // On sauvegarde les états de la machine qu'on va modifier
  glPushAttrib(GL_VIEWPORT_BIT);
  glPushAttrib(GL_ENABLE_BIT);

  // On definit la taille du z-buffer correspondant à la taille de la
  glViewport(0, 0, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);

  // Il n'a que le z-buffer qui nous intéresse
  glClear(GL_DEPTH_BUFFER_BIT);
 
  // On définit un volume de projection qui contiendra la scène.
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluPerspective(lightFovy, lightAspect, lightNearPlane, lightFarPlane);
  glMatrixMode(GL_MODELVIEW);

  // On se place à la position de la lumière,
  // la composante en z des pixels sera donc en relation avec
  // la distance de ceux-ci à la lumière
  glPushMatrix();
  glLoadIdentity();
  gluLookAt(lumPosition[0], lumPosition[1], lumPosition[2],
            0, 0, 0,
            lumUp[0], lumUp[1], lumUp[2]);

  // On désactive la depth texture
  // (il vaut mieux, vu qu'on ne l'a pas encore calculée)
  glDisable(GL_TEXTURE_2D);

  // On désactive l'éclairement pour aller plus vite
  // (on se rappelle que seul le z-buffer nous intéresse)
  glDisable(GL_LIGHTING);

  // On décale un peu la profondeur des fragments afin d'éviter l'aliasing
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(5, 0);

  dessineScene();
  //dessineSol();

  glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 0, 0,
                   SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT, 0);
  // On remet les chose en ordre
  glPopMatrix();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  // On restaure la texture, l'éclairement et le polygon offset
  glPopAttrib();
  // On restaure le viewport
  glPopAttrib();

  /* On va générer la même transformation que celle qui nous a permis
   * de créer la shadow map. Une seule différence : les coordonnées dans la texture
   * devant être entre 0 et 1, on fait une mise à l'échelle.
   * Afin de créer la matrice de transformation on s'aide d'OpenGL
   */
  glPushMatrix();

  glLoadIdentity();
  glTranslated(0.5, 0.5, 0.5);
  glScaled(.5, .5, .5);
  gluPerspective(lightFovy, lightAspect, lightNearPlane, lightFarPlane);
  gluLookAt(lumPosition[0], lumPosition[1], lumPosition[2],
            0, 0, 0,
            lumUp[0], lumUp[1], lumUp[2]);

  // On récupère la matrice de transformation
  glGetDoublev(GL_MODELVIEW_MATRIX, shadowMapProjectionMatrix);

  glPopMatrix();

  /* La matice shadowMapProjectionMatrix contient maintenant la transformation permettant
   * de relier le pixel à la texture */

  /* Les coordonnées utilisées pour calculer celles de la textures sont
     les coordonnées du sommet dans l'univers affectées par la matrice
     MODELVIEW (afin de tenir compte des éventuels Translate, Rotate, etc...)
  */
  glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
  glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
  glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
  glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
 
  /* On transpose la matrice puisque celle-ci est donnée par colonnes, or
   * pour calculer les coordonnées dans la texture on a besoin des lignes.
   * Par exemple, pour calculer r, on effectue :
   * r = m[1,1] * x + m[1,2] * y + m[1,3] * z + m[1,4] * w
   */
  transposeMatrix(shadowMapProjectionMatrix);

}

/**
 * Réalise un gluLookAt en fonction de la position
 * (rho, theta, phi) de l'observateur.
 */
void positionneObservateur() {
  double posObsx, posObsy, posObsz;
  double basHautx, basHauty, basHautz;

  posObsx = rho * cos(degRad(phi)) * cos(degRad(theta));
  posObsy = rho * sin(degRad(phi));
  posObsz = rho * cos(degRad(phi)) * sin(degRad(theta));

  basHautx = sin(degRad(-phi)) * cos(degRad(theta));
  basHauty = cos(degRad(-phi));
  basHautz = sin(degRad(-phi)) * sin(degRad(theta));

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(posObsx, posObsy, posObsz, 0, 0, 0, basHautx, basHauty, basHautz);
  glLightfv(GL_LIGHT0, GL_POSITION, lumPosition);

}

void reshape(int w, int h) {
  glViewport(0, 0, w, h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, (GLdouble) w / h, 8.5, 50);    
  glMatrixMode(GL_MODELVIEW);
}

void init_SDL() {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Erreur à l'initialisation de la vidéo : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  
  fenetre = SDL_CreateWindow("SDL B-A-BA Gestion des unités de textures",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

void init_GL(void) {
  glEnable(GL_DEPTH_TEST);
  
  glClearColor(0., 0., 0., 0.);

  glEnable(GL_LIGHTING);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lumZero);
  
  glEnable(GL_LIGHT0);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, 1, 8.5, 50);
  glMatrixMode(GL_MODELVIEW);

  positionneObservateur();
  
  quad = gluNewQuadric();

  // Par défaut on travaillera sur l'unité de texture 0
  glActiveTexture(GL_TEXTURE0);

  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  // On crée un objet texture pour la shadow map
  glGenTextures(1,&shadowMap);
  glBindTexture(GL_TEXTURE_2D, shadowMap);
  /* On génère toutes les coordonnées */
  glEnable(GL_TEXTURE_GEN_S);
  glEnable(GL_TEXTURE_GEN_T);
  glEnable(GL_TEXTURE_GEN_R);
  glEnable(GL_TEXTURE_GEN_Q);

  /* Un r plus grand que la valeur de la texture indique un pixel derrière
     un pixel éclairé */
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

  /* GL_COMPARE_R_TO_TEXTURE prend le r/q généré puis
   * le compare à celui qui est dans la texture
   */
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
  glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  // On réalise une interpolation pour avoir des ombres un petit peu plus douces
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  
  // Produit des artefacts sur le devant des boules, pourquoi ???
  /*
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  float borderColor[4] = {1, 1, 1, 1};
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
  //glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 0, 0,
  //                 SHADOW_MAP_WIDTH + 2, SHADOW_MAP_HEIGHT + 2, 1);
  */
  
  creeShadowMap();

  initTexture();
}

void GL_Quit() {
  gluDeleteQuadric(quad);
}

/**
 * Affichage de la scene : on n'affiche que les trois sphère, pas le sol.
 */
void dessineScene() {
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpeculaire);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, matShininess);

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matRouge);
  glTranslatef(1., .5, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matVert);
  glTranslatef(-1., .5, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matBleu);
  glTranslatef(0., .5, -1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

}

void dessineSol() {
  // On active l'unité de texture qui contient la texture du sol
  glActiveTexture(GL_TEXTURE1);
  // On active le placage de texture
  glEnable(GL_TEXTURE_2D);

  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matGris);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpeculaire);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, matShininess);

  glNormal3f(0, 1, 0);
  float pas = 1;
  float dim = 6;
  for (float i = -dim; i < dim; i += pas) {
    glBegin(GL_QUAD_STRIP);
    for (float j = -dim; j <= dim; j += pas) {
      glMultiTexCoord2d(GL_TEXTURE1, (j + dim) / dim, (i + dim) / dim);
      glVertex3f(j, 0, i);
      glMultiTexCoord2d(GL_TEXTURE1, (j + dim) / dim, (i + pas + dim) / dim);
      glVertex3f(j, 0, i + pas);
    }
    glEnd();
  }
  // On n'a plus besoin de la texture du sol
  glDisable(GL_TEXTURE_2D);
  glActiveTexture(GL_TEXTURE0);
}

void drawShadowMap() {
  glActiveTexture(GL_TEXTURE2);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  glBindTexture(GL_TEXTURE_2D, shadowMap);
  glEnable(GL_TEXTURE_2D);

  glPushAttrib(GL_TEXTURE_BIT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
 
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glBegin(GL_QUADS);
  glMultiTexCoord2d(GL_TEXTURE2, 0, 0); glVertex2i(-1, -1);
  glMultiTexCoord2d(GL_TEXTURE2, 1, 0); glVertex2i( 1, -1);
  glMultiTexCoord2d(GL_TEXTURE2, 1, 1); glVertex2i( 1,  1);
  glMultiTexCoord2d(GL_TEXTURE2, 0, 1); glVertex2i(-1,  1);
  glEnd();

  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  glDisable(GL_TEXTURE_2D);
  glActiveTexture(GL_TEXTURE0);

  glPopAttrib();
}

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (drawShadows) {
    drawShadowMap();
    SDL_GL_SwapWindow(fenetre);
    
    if ((erreur = glGetError()) != GL_NO_ERROR) {
      printf("Erreur à la fin de l'affichage\n");
    }

    return;
  } 

  /*
   * Première passe :
   * affichage de la scène avec la lumière ambiante.
   */
  glLightfv(GL_LIGHT0, GL_AMBIENT, lumiereAmbiante);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lumZero);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lumZero);
 
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // On ne s'occupe pas de la shadow map
  glDisable(GL_TEXTURE_2D);

  // On ajoute un léger décalage en profondeur afin qu'on ne tombe
  // pas exactement sur la même position lors de la seconde passe
  // (auquel cas le fragment serait écarté)
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(5, 5);

  dessineSol();
  dessineScene();

  glDisable(GL_POLYGON_OFFSET_FILL);

  /*
   * Deuxième passe :
   * affichage de la scène avec la lumière ambiente, diffuse et spéculaire
   * pour les zones qui ne sont pas à l'ombre.
   */
  glLightfv(GL_LIGHT0, GL_AMBIENT, lumiereAmbiante);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lumiere);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lumiere);

  /*
   * On ne va garder que les fragments qui ont un alpha supérieur à 0.9,
   * cela correspondra aux framents qui sont éclairés (les autres ont
   * un alpha mais aussi des R, G et B à 0).
   */
  glAlphaFunc(GL_GREATER, 0.9);
  glEnable(GL_ALPHA_TEST);
  
  glEnable(GL_TEXTURE_2D);

  /* On calcule les coordonnées à partir d'une transformation qu'on spécifie */
  glTexGendv(GL_S, GL_EYE_PLANE, &shadowMapProjectionMatrix[0]);
  glTexGendv(GL_T, GL_EYE_PLANE, &shadowMapProjectionMatrix[4]);
  glTexGendv(GL_R, GL_EYE_PLANE, &shadowMapProjectionMatrix[8]);
  glTexGendv(GL_Q, GL_EYE_PLANE, &shadowMapProjectionMatrix[12]);

  dessineSol();
  dessineScene();

  glDisable(GL_ALPHA_TEST);

  SDL_GL_SwapWindow(fenetre);

  if ((erreur = glGetError()) != GL_NO_ERROR) {
    printf("Erreur à la fin de l'affichage\n");
  }
}

/**
 * Fonction appelée lorsqu'on bouge la souris.
 */
void motion(int depx, int depy) {
  if (boutonGaucheAppuye) {
    theta += depx;
    phi += depy;
    positionneObservateur();
  }
}

/**
 * Fonction appelée lorsqu'on appuie sur un bouton de la souris.
 */
void mouseButton(SDL_MouseButtonEvent button) {
  if (button.button == SDL_BUTTON_LEFT) {
    if (button.state == SDL_PRESSED) {
      // On va déplacer l'observateur
      boutonGaucheAppuye = true;
    } else if (button.state == SDL_RELEASED) {
      boutonGaucheAppuye = false;
    }
  }
}

int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case 's':
      drawShadows = (drawShadows + 1) % 2;
      break;
    case SDLK_ESCAPE:
      return 0;
    default:
      break;
    }
  }
  
  return 1;
}

int main(void) {
  init_SDL();
  init_GL();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les évènements de la fenetre
  while (continuer) {
    SDL_WaitEvent(&event);
    switch (event.type) {
    case SDL_QUIT:
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      continuer = keyboard(&event);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_RESIZED:
        reshape(event.window.data1, event.window.data2);
        break;
      }
      break;
    case SDL_MOUSEMOTION:
      {
        SDL_Event event_temp;
        int depx = event.motion.xrel;
        int depy = event.motion.yrel;
        while (SDL_PeepEvents(&event_temp, 1, SDL_PEEKEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION) > 0) {
          SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION);
          depx += event.motion.xrel;
          depy += event.motion.yrel;
        }
        motion(depx, depy);
        break;
      }
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
      mouseButton(event.button);
      break;
    }
    display();
  }
  
  GL_Quit();
  SDL_Quit();
  
  return EXIT_SUCCESS;
}
