/**
 * Petit programme d'exemple de fonctionnement de la SDL avec OpenGL.
 */

#include <stdlib.h>
#include <stdio.h>

#include <SDL.h>
#include <GL/glu.h>
#include <GL/gl.h>

typedef struct {
  float x;
  float y;
  float z;
} point;

SDL_Window *fenetre;
SDL_GLContext context;

void init_SDL(void);
void init_GL(void);
void printGLInfos(void);
void display(void);
int keyboard(SDL_Event * event);

struct cuboid {
  float width;
  float height;
  float depth;
  point *points;
};

struct cuboid *createCuboid(float w, float h, float d) {
  point *v = malloc(8 * sizeof (point));
  point vf = { -w/2.0, h/2.0, d/2.0 };
  point vb = { -w/2.0, h/2.0, -d/2.0 };
  v[0] = vf; v[4] = vb;
  vf.x = vb.x = w/2.0;
  v[1] = vf; v[5] = vb;
  vf.y = vb.y = -h/2.0;
  v[2] = vf; v[6] = vb;
  vf.x = vb.x = -w/2.0;
  v[3] = vf; v[7] = vb;
  struct cuboid *c = malloc(sizeof (struct cuboid));
  c->width = w;
  c->height = h;
  c->depth = d;
  c->points = v;
  return c;
}

void displayPoint(point p) {
  glVertex3f(p.x, p.y, p.z);
}

void displayCuboidFace(struct cuboid *cuboid, int a, int b, int c, int d) {
  displayPoint(cuboid->points[a]);
  displayPoint(cuboid->points[b]);
  displayPoint(cuboid->points[c]);
  displayPoint(cuboid->points[d]);
}

void displayCuboid(struct cuboid *cuboid) {
  glBegin(GL_QUADS);
  glColor3f(1.0, 0.0, 0.0);
  displayCuboidFace(cuboid, 0, 1, 2, 3);
  glColor3f(0.0, 1.0, 0.0);
  displayCuboidFace(cuboid, 1, 2, 6, 5);
  glColor3f(0.0, 0.0, 1.0);
  displayCuboidFace(cuboid, 1, 5, 4, 0);
  glColor3f(1.0, 1.0, 0.0);
  displayCuboidFace(cuboid, 7, 6, 5, 4);
  glColor3f(0.0, 1.0, 1.0);
  displayCuboidFace(cuboid, 0, 3, 7, 4);
  glColor3f(1.0, 0.0, 1.0);
  displayCuboidFace(cuboid, 7, 6, 2, 3);
  glEnd();
}

void freeCuboid(point *cuboid) {
  free(cuboid);
}

struct robot_t {
  struct cuboid *arm;
  struct cuboid *forarm;
  struct cuboid *finger1;
  struct cuboid *finger2;
  float finger_spacing;
};

void restoreMatrix() {
  glPopMatrix();
  glPushMatrix();
}

struct robot_t *robot;

void displayRobot(struct robot_t *rbt) {
  glRotatef(1.0, 1.0, 1.0, 0.0);
  glPushMatrix();
  glTranslatef(0.0, 0.0, -rbt->arm->depth*3.0);
  displayCuboid(rbt->arm);
  glTranslatef(0.0, rbt->arm->height/2.0, 0.0);
  glRotatef(50.0, 0.0, 0.0, 1.0);
  glTranslatef(0.0, rbt->forarm->height/2.0, 0.0);
  displayCuboid(rbt->forarm);
  glTranslatef(0.0, rbt->forarm->height/2.0, 0.0);
  glRotatef(50.0, 0.0, 0.0, 1.0);
  glTranslatef(0.0, rbt->finger1->height/2.0, 0.0);
  glPushMatrix();
  glTranslatef(0.0, 0.0, -rbt->finger_spacing/2.0);
  displayCuboid(rbt->finger1);
  restoreMatrix();
  glTranslatef(0.0, 0.0, rbt->finger_spacing/2.0);
  displayCuboid(rbt->finger2);
  glPopMatrix();
  glPopMatrix();
}

void freeRobot(struct robot_t *rbt) {
  free(rbt->arm);
  free(rbt->forarm);
  free(rbt->finger1);
  free(rbt->finger2);
  free(rbt);
}

point *cube;

int main(int argc, char **argv) {
  if (argc != 1) {
    fprintf(stderr, "Usage %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  robot = malloc(sizeof (struct robot_t));
  robot->arm = createCuboid(0.2, 0.6, 0.2);
  robot->forarm = createCuboid(0.15, 0.3, 0.15);
  robot->finger1 = createCuboid(0.07, 0.15, 0.07);
  robot->finger2 = createCuboid(0.07, 0.15, 0.07);
  robot->finger_spacing = 0.1;

  init_SDL();
  init_GL();

  printGLInfos();

  SDL_ShowWindow(fenetre);
    
  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les évènements de la fenetre
  while (continuer) {
    // On attend le prochain évènement
    SDL_WaitEvent(&event);
    // On traite l'évènement
    switch (event.type) {
    case SDL_QUIT:
      // On a fermé la fenetre
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      // On a appuyé sur une touche
      continuer = keyboard(&event);
      break;
    }
    display();
  }
  
  // C'est fini : on libère les ressources proprement
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();
  freeRobot(robot);

  return EXIT_SUCCESS;
}

/**
 * Initialisation de la SDL, création du contexte OpenGL et ouverture de la fenetre.
 */
void init_SDL(void) {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Couldn't initialize video driver: %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    
  fenetre = SDL_CreateWindow("SDL B-A-BA",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenÃªtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL (SDL)\n");
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

/**
 * Initialisation des états du contexte OpenGL.
 */
void init_GL(void) {
  glClearColor(1., 1., 1., 0.);
  glEnable(GL_DEPTH_TEST);
}

/**
 * Affichage des informations relatives à OpenGl
 */
void printGLInfos(void) {
  const GLubyte* string;
  
  string = glGetString(GL_VENDOR);
  printf("Marque fournissant l'implémentation OpenGL : %s\n", string);

  string = glGetString(GL_RENDERER);
  printf("Configuration graphique : %s\n", string);

  string = glGetString(GL_VERSION);
  printf("Version : %s\n", string);
}



/**
 * Affichage de la scène OpenGL.
 */
void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /* glRotatef(1.0, 1.0, 1.0, 1.0); */
  
  displayRobot(robot);

  SDL_GL_SwapWindow(fenetre);

  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans display : %s\n", gluErrorString(erreur));
  }

}

/**
 * Gestion du clavier.
 * @param event l'évènement SDL ayant déclenché l'appel à la fonction
 * (doit etre de type SDL_KEYDOWN).
 */
int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    default:
      break;
    }
  }
  
  return 1;
}

