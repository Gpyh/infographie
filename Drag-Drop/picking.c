/**
 * Exemple de s�lection d'objet � la souris.
 * On d�place l'observateur � l'aide du bouton gauche de la souris.
 * On s�lectionne un objet � l'aide du bouton droit.
 */

#include <stdbool.h>
#include <math.h>

#include <SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifndef M_PI
#define M_PI 3.141592654
#endif

#define degRad(a) (((double) a) * M_PI / 180.)

/* Taille du tampon de s�lection */
#define BUFSIZE 512

/* Position de l'observateur */
float rho = 15;
float theta = 90;
float phi = 0;

/* Gestion de la souris */
int boutonGaucheAppuye = false;

/* Objet s�lectionn� */
int objetSelectionne = 0;

/* Lumi�re (fixe par rapport � la sc�ne) */
GLfloat lumPosition[4] = {3., 3., 0., 1.};
GLfloat lumiere[4] = {1., 1., 1., 1.};
GLfloat lumZero[4] = {0., 0., 0., 1.};
GLfloat matColor[4] = {.2, .2, .2, 1.};
GLfloat matSelectionne[4] = {.2, .0, .0, 1.};
GLfloat matSpeculaire[4] = {1., 1., 1., 1.};
GLfloat matShininess = 50.;

/* Une quadrique pour la GLU */
GLUquadric * quad;

SDL_Window *fenetre;
SDL_GLContext context;

GLenum erreur;

/**
 * R�alise un gluLookAt en fonction de la postion
 * (rho, theta, phi) de l'observateur.
 */
void positionneObservateur() {
  double posObsx, posObsy, posObsz;
  double basHautx, basHauty, basHautz;

  /* Calcul de la nouvelle position de l'observateur */
  posObsx = rho * cos(degRad(phi)) * cos(degRad(theta));
  posObsy = rho * sin(degRad(phi));
  posObsz = rho * cos(degRad(phi)) * sin(degRad(theta));

  /* Calcul du nouveau vecteur bas-haut */
  basHautx = sin(degRad(-phi)) * cos(degRad(theta));
  basHauty = cos(degRad(-phi));
  basHautz = sin(degRad(-phi)) * sin(degRad(theta));

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(posObsx, posObsy, posObsz, 0, 0, 0, basHautx, basHauty, basHautz);
}

void reshape(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, (GLdouble) w / h, 8.5, 50);
  
  glMatrixMode(GL_MODELVIEW);
}

void init_SDL() {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Erreur � l'initialisation de la vid�o : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8) != 0) {
    fprintf(stderr, "Imposible de fixer le stencil buffer : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }

  fenetre = SDL_CreateWindow("B-A-BA Drag and Drop",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la cr�ation de la fen�tre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la cr�ation du contexte OpenGL\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

void init_GL(void) {
  glEnable(GL_DEPTH_TEST);
  
  glClearColor(0., 0., 0., 0.);

  glEnable(GL_LIGHTING);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lumZero);
  
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_AMBIENT, lumiere);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lumiere);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lumiere);
  
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpeculaire);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, matShininess);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, 1, 8.5, 50);
  glMatrixMode(GL_MODELVIEW);

  positionneObservateur();
  
  quad = gluNewQuadric();
}

void GL_Quit() {
  gluDeleteQuadric(quad);
}

/**
 * Affichage de la scene selon deux modes :
 * <ul>
 * <li>GL_RENDER affiche la sc�ne</li>
 * <li>GL_SELECT s�lectionne les objets pr�sents dans le volume de vue/li>
 * </ul>
 */
void dessineScene(GLenum mode) {
  if (mode == GL_RENDER) {
    glEnable(GL_LIGHTING);
    glLightfv(GL_LIGHT0, GL_POSITION, lumPosition);
  }
  else {
    glDisable(GL_LIGHTING);
  }
  
  /* Sol */
  if (mode == GL_SELECT) {
    // On associe le nom "1" au sol
    glLoadName(1);
  }
  if (objetSelectionne == 1) {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matSelectionne);
  } else {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matColor);
  }
  glNormal3f(0, 1, 0);
  float pas = 1;
  for (float i = -3; i < 3; i += pas) {
    glBegin(GL_QUAD_STRIP);
    for (float j = -3; j <= 3; j += pas) {
      glVertex3f(j, -1.2, i);
      glVertex3f(j, -1.2, i + pas);
    }
    glEnd();
  }
  
  glPushMatrix();
  if (mode == GL_SELECT) {
    // On associe le nom "2" � la premi�re sph�re
    glLoadName(2);
  }
  if (objetSelectionne == 2) {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matSelectionne);
  } else {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matColor);
  }
  glTranslatef(1., -.7, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  if (mode == GL_SELECT) {
    // On associe le nom "3" � la deuxi�me sph�re
    glLoadName(3);
  }
  if (objetSelectionne == 3) {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matSelectionne);
  } else {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matColor);
  }
  glTranslatef(-1., -.7, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  if (mode == GL_SELECT) {
    // On associe le nom "4" � la troisi�me sph�re
    glLoadName(4);
  }
  if (objetSelectionne == 4) {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matSelectionne);
  } else {
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matColor);
  }
  glTranslatef(0., -.7, -1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

}


void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  dessineScene(GL_RENDER);
  
  SDL_GL_SwapWindow(fenetre);

  if ((erreur = glGetError()) != GL_NO_ERROR) {
    printf("Erreur � la fin de l'affichage\n");
  }
}

/**
 * Fonction appel�e lorsqu'on bouge la souris.
 */
void motion(int depx, int depy) {
  if (boutonGaucheAppuye) {
    theta += depx;
    phi += depy;
    positionneObservateur();
  }
}

/**
 * Traitement des informations re�ues d'OpenGL lors du "picking".
 * @param hits nombre d'objets qui ont �t� s�lectionn�es.
 * @param buffer tableau contenant les informations concernant les objets.
 */
void processHits(GLint hits, GLuint buffer[]) {
  unsigned int  j;
  int i;
  GLuint names, *ptr;
  GLfloat z1, /* z2, */ z_courant;

  // Valeur par d�faut indiquant qu'aucun objet n'a �t� trait�
  objetSelectionne = 0;

  // Parcours du tableau
  ptr = (GLuint *) buffer ;
  for (i = 0; i < hits; i++) {
    // On r�cup�re le nombre de noms associ�s � l'objet
    names = *ptr;
    ptr++;

    // On r�cup�re l'extension en z de l'objet
    // (z min et z max)
    // z est une valeur comprise entre [0, 1] multipli�e par 2^32 - 1
    // 0 indique un objet dans le plan near.
    z1 = (GLfloat) (*ptr);
    ptr++;
    // z2 = (GLfloat) (*ptr);
    ptr++;

    // On r�cup�re les noms associ�s � l'objet
    for (j = 0 ; j < names; j++) {
      if ((*ptr) != 0 && (!objetSelectionne || z_courant > z1)) {
        z_courant = z1;
        objetSelectionne = *ptr;
      }
      ptr++;
    }
  }
}

/**
 * Calcul de l'objet qui a �t� s�lectionn� par la souris.
 */
void calculObjetSelectionne(int x, int y) {
  GLuint selectBuf[BUFSIZE];
  GLint hits;
  GLint viewport[4];

  // On r�cup�re les dimensions du viewport
  glGetIntegerv(GL_VIEWPORT,viewport);

  // On indique le tableau devant recevoir les propri�t�s des objets s�lectionn�s
  glSelectBuffer(BUFSIZE, selectBuf);

  // On place la machine OpenGL en mode de s�lection
  glRenderMode(GL_SELECT);
  // On initialise la pile des noms
  glInitNames();
  glPushName(0);

  // On d�finit un volume de projection restreint aux pixels sous la souris
  // (et un peu � c�t�).
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3] - y), 5.0, 5.0, viewport) ;
  gluPerspective(30, (GLdouble) viewport[2] / viewport[3], 8.5, 50);

  // On calcule la sc�ne pour r�cup�rer les objets qui sont sous la souris
  glMatrixMode(GL_MODELVIEW);
  dessineScene(GL_SELECT);

  // On restore la projection
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  // On analyse les informations transmises par OpenGL
  hits = glRenderMode(GL_RENDER);
  processHits(hits, selectBuf);

}

/**
 * Fonction appel�e lorsqu'on appuie sur un bouton de la souris.
 */
void mouseButton(SDL_MouseButtonEvent button) {
  if (button.button == SDL_BUTTON_LEFT && button.state == SDL_PRESSED) {
    // On va d�placer l'observateur
    boutonGaucheAppuye = true;
  } else if (button.button == SDL_BUTTON_LEFT && button.state == SDL_RELEASED) {
    boutonGaucheAppuye = false;
  } else if (button.button == SDL_BUTTON_RIGHT && button.state == SDL_PRESSED) {
    calculObjetSelectionne(button.x, button.y);
  }
}

int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    default:
      break;
    }
  }
  
  return 1;
}

int main(void) {
  init_SDL();
  init_GL();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les �v�nements de la fenetre
  while (continuer) {
    // On attend le prochain �v�nement
    SDL_WaitEvent(&event);
    // On traite l'�v�nement
    switch (event.type) {
    case SDL_QUIT:
      // On a ferm� la fenetre
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      // On a appuy� sur une touche
      continuer = keyboard(&event);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_RESIZED:
        reshape(event.window.data1, event.window.data2);
        break;
      }
      break;
    case SDL_MOUSEMOTION:
      {
        SDL_Event event_temp;
        int depx = event.motion.xrel;
        int depy = event.motion.yrel;
        while (SDL_PeepEvents(&event_temp, 1, SDL_PEEKEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION) > 0) {
          SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION);
          depx += event.motion.xrel;
          depy += event.motion.yrel;
        }
        motion(depx, depy);
        break;
      }
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
      mouseButton(event.button);
      break;
    }
    display();
  }
  
  // C'est fini : on lib�re les ressources proprement
  GL_Quit();
  SDL_Quit();
  
  return EXIT_SUCCESS;
}
