/**
 * Exemple de projection d'ombres sur un plan (ici le sol)
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include <SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifndef M_PI
#define M_PI 3.141592654
#endif

#define degRad(a) (((double) a) * M_PI / 180.)

/* Position de l'observateur */
float rho = 15;
float theta = 90; // angle (Ox, OP') (P' est le projeté de P dans le plan Oxz)
float phi = 30;   // angle (OP', OP)

/* Gestion de la souris */
int boutonGaucheAppuye = false;

/* Lumière (fixe par rapport à la scène) */
GLfloat lumPosition[4] = {3., 3., 0., 1.};

GLfloat lumiere[4] = {1., 1., 1., 1.};
GLfloat lumZero[4] = {0., 0., 0., 1.};
GLfloat matGris[4] = {.4, .4, .4, 1.};
GLfloat matRouge[4] = {.2, .0, .0, 1.};
GLfloat matVert[4] = {.0, .2, .0, 1.};
GLfloat matBleu[4] = {.0, .0, .2, 1.};
GLfloat matSpeculaire[4] = {1., 1., 1., 1.};
GLfloat matShininess = 50.;

/* Une quadrique pour la GLU */
GLUquadric * quad;

SDL_Window *fenetre;
SDL_GLContext context;

GLenum erreur;

/**
 * On crée une matrice de projection par rapport à un point (lightpos)
 * sur un plan (de normale groundplane).
 * J'ai été un peu fainéant, je l'ai récupérée à partir d'un exemple
 * fourni par Mark J. Kilgard
 */
void shadowMatrix(GLfloat shadowMat[4][4],
                  GLfloat groundplane[4],
                  GLfloat lightpos[4]) {
  GLfloat dot;

  dot = groundplane[0] * lightpos[0] +
    groundplane[1] * lightpos[1] +
    groundplane[2] * lightpos[2] +
    groundplane[3] * lightpos[3];
  
  shadowMat[0][0] = dot - lightpos[0] * groundplane[0];
  shadowMat[1][0] = 0.f - lightpos[0] * groundplane[1];
  shadowMat[2][0] = 0.f - lightpos[0] * groundplane[2];
  shadowMat[3][0] = 0.f - lightpos[0] * groundplane[3];

  shadowMat[0][1] = 0.f - lightpos[1] * groundplane[0];
  shadowMat[1][1] = dot - lightpos[1] * groundplane[1];
  shadowMat[2][1] = 0.f - lightpos[1] * groundplane[2];
  shadowMat[3][1] = 0.f - lightpos[1] * groundplane[3];

  shadowMat[0][2] = 0.f - lightpos[2] * groundplane[0];
  shadowMat[1][2] = 0.f - lightpos[2] * groundplane[1];
  shadowMat[2][2] = dot - lightpos[2] * groundplane[2];
  shadowMat[3][2] = 0.f - lightpos[2] * groundplane[3];

  shadowMat[0][3] = 0.f - lightpos[3] * groundplane[0];
  shadowMat[1][3] = 0.f - lightpos[3] * groundplane[1];
  shadowMat[2][3] = 0.f - lightpos[3] * groundplane[2];
  shadowMat[3][3] = dot - lightpos[3] * groundplane[3];
}

/**
 * Réalise un gluLookAt en fonction de la position
 * (rho, theta, phi) de l'observateur.
 */
void positionneObservateur() {
  double posObsx, posObsy, posObsz;
  double basHautx, basHauty, basHautz;

  /* Calcul de la nouvelle position de l'observateur */
  posObsx = rho * cos(degRad(phi)) * cos(degRad(theta));
  posObsy = rho * sin(degRad(phi));
  posObsz = rho * cos(degRad(phi)) * sin(degRad(theta));

  /* Calcul du nouveau vecteur bas-haut */
  basHautx = sin(degRad(-phi)) * cos(degRad(theta));
  basHauty = cos(degRad(-phi));
  basHautz = sin(degRad(-phi)) * sin(degRad(theta));

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(posObsx, posObsy, posObsz, 0, 0, 0, basHautx, basHauty, basHautz);
}

void reshape(int w, int h) {
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(30, (GLdouble) w / h, 8.5, 50);    
    glMatrixMode(GL_MODELVIEW);
}

void init_SDL() {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Erreur à l'initialisation de la vidéo : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8) != 0) {
    fprintf(stderr, "Imposible de fixer le stencil buffer : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }

  fenetre = SDL_CreateWindow("B-A-BA Ombres",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

void init_GL(void) {
  glEnable(GL_DEPTH_TEST);
  
  glClearColor(0., 0., 0., 1.);

  glEnable(GL_LIGHTING);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lumZero);
  
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_AMBIENT, lumiere);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lumiere);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lumiere);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, 1, 8.5, 50);
  glMatrixMode(GL_MODELVIEW);

  positionneObservateur();
  
  quad = gluNewQuadric();
}

void GL_Quit() {
  gluDeleteQuadric(quad);
}

/**
 * Affichage de la scene 
 */
void dessineScene() {
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpeculaire);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, matShininess);

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matRouge);
  glTranslatef(1., .5, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matVert);
  glTranslatef(-1., .5, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matBleu);
  glTranslatef(0., .5, -1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

}

void dessineSol() {
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matGris);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpeculaire);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, matShininess);

  glNormal3f(0, 1, 0);
  float pas = 1;
  for (float i = -3; i < 3; i += pas) {
    glBegin(GL_QUAD_STRIP);
    for (float j = -3; j <= 3; j += pas) {
      glVertex3f(j, 0, i);
      glVertex3f(j, 0, i + pas);
    }
    glEnd();
  }

}

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  glLightfv(GL_LIGHT0, GL_POSITION, lumPosition);

  GLfloat projectionOmbres[4][4];
  GLfloat normaleSol[4] = {0, 1, 0, 0};
  shadowMatrix(projectionOmbres, normaleSol, lumPosition);

  // On mémorise là on on voudra afficher les ombres
  // Normalement il faudrait faire deux passes :
  // - une passe où on dessine le sol et on met des 1 dans le stencil buffer
  // - une passe où on dessine les sphères et on met des zéro dans le stencil buffer
  glEnable(GL_STENCIL_TEST);
  glStencilFunc(GL_ALWAYS, 1, 0xff);
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

  dessineSol();
  dessineScene();

  // On n'affiche que si 0 est inf. à ce qui est dans le stencil buffer
  glStencilFunc(GL_LESS, 0, 0xff); 
  glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(-2.0, -1.0);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_LIGHTING);
  glColor4f(0.0, 0.0, 0.0, 0.5);

  glPushMatrix();
  /* On projette les ombres */
  glMultMatrixf((GLfloat *) projectionOmbres);
  dessineScene();
  glPopMatrix();


  glDisable(GL_BLEND);
  glEnable(GL_LIGHTING);
  glDisable(GL_POLYGON_OFFSET_FILL);
  glDisable(GL_STENCIL_TEST);

  SDL_GL_SwapWindow(fenetre);

  if ((erreur = glGetError()) != GL_NO_ERROR) {
    printf("Erreur à la fin de l'affichage\n");
  }
}

/**
 * Fonction appelée lorsqu'on bouge la souris.
 */
void motion(int depx, int depy) {
  if (boutonGaucheAppuye) {
    theta += depx;
    phi += depy;
    positionneObservateur();
  }
}

/**
 * Fonction appelée lorsqu'on appuie sur un bouton de la souris.
 */
void mouseButton(SDL_MouseButtonEvent button) {
  if (button.button == SDL_BUTTON_LEFT) {
    if (button.state == SDL_PRESSED) {
      // On va déplacer l'observateur
      boutonGaucheAppuye = true;
    } else if (button.state == SDL_RELEASED) {
      boutonGaucheAppuye = false;
    }
  }
}

int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    default:
      break;
    }
  }
  
  return 1;
}

int main(void) {
  init_SDL();
  init_GL();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les évènements de la fenetre
  while (continuer) {
    SDL_WaitEvent(&event);
    switch (event.type) {
    case SDL_QUIT:
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      continuer = keyboard(&event);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_RESIZED:
        reshape(event.window.data1, event.window.data2);
        break;
      }
      break;
    case SDL_MOUSEMOTION:
      {
        SDL_Event event_temp;
        int depx = event.motion.xrel;
        int depy = event.motion.yrel;
        while (SDL_PeepEvents(&event_temp, 1, SDL_PEEKEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION) > 0) {
          SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION);
          depx += event.motion.xrel;
          depy += event.motion.yrel;
        }
        motion(depx, depy);
        break;
      }
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
      mouseButton(event.button);
      break;
    }
    display();
  }
  
  GL_Quit();
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();
  
  return EXIT_SUCCESS;
}
