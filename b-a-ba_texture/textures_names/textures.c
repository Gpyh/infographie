#include <SDL.h>
#include <SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>

// Les noms (OpenGL) des textures qu'on va manipuler
GLuint tex[2];

SDL_Window *fenetre;
SDL_GLContext context;

void initGL() {
  glClearColor(0.,0.,0.,1.0);
  glEnable(GL_DEPTH_TEST);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60, 1, 8, 100);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(0, 5, 20, 0, 0, 0, 0, 1, 0);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);

  GLfloat intensite[] = {1., 1., 1., 1.};
  glLightfv(GL_LIGHT0, GL_AMBIENT, intensite);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, intensite);
  glLightfv(GL_LIGHT0, GL_SPECULAR, intensite);

  GLfloat pos[] = {2, 5, 3, 1};
  glLightfv(GL_LIGHT0, GL_POSITION, pos);

  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
}

void defTexture(const char * filename) {
    // On fixe les paramètres de la texture,
    // i.e. comment on calcule les coordonnées pixels -> texels.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // On charge l'image qui va nous servir de texture via la SDL
    SDL_Surface* image = IMG_Load(filename);
    if(!image) {
      fprintf(stderr, "Impossible de charger l'image : %s\n", IMG_GetError());
      exit(EXIT_FAILURE);
    }

    // On verrouille l'image pendant qu'on l'utilise
    if (SDL_MUSTLOCK(image)) {
        SDL_LockSurface(image);
    }

    // On envoie le tableau contenant les texels de la texture à OpenGL
    if (image->format->BytesPerPixel == 3) {
      // L'image d'origine n'a que trois composantes
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
    } else if (image->format->BytesPerPixel == 4) {
      // L'image d'origine à quatre composantes
      // (la dernière est le paramètre alpha qui va nous servir à gérer la transparence)
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
    } else {
      fprintf(stderr, "Nombre de composants de l'image différent de 3 et 4\n");
      exit(EXIT_FAILURE);
    }

    // On libère les ressources occupées par l'image
    if (SDL_MUSTLOCK(image)) {
        SDL_UnlockSurface(image);
    }
    SDL_FreeSurface(image);
 }

void initTexture() {
    glEnable(GL_TEXTURE_2D);
    glGenTextures(2, tex);

    // On définit la première texture
    glBindTexture(GL_TEXTURE_2D, tex[0]);
    defTexture("marblewhite.bmp");

    // On définit la deuxième texture
    glBindTexture(GL_TEXTURE_2D, tex[1]);
    defTexture("checker.bmp");

    // On spécifie comment les textures seront plaquées sur les facettes
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    GLenum erreur;
    if ((erreur = glGetError()) != GL_NO_ERROR) {
        fprintf(stderr, "Erreur OpenGL dans setTexture : %s\n", gluErrorString(erreur));
    }

}

void cube() {
  glBindTexture(GL_TEXTURE_2D, tex[0]);
  glBegin(GL_QUADS);
  // face avant
  glNormal3f(0, 0, 1);
  glTexCoord2f(0., 0.); glVertex3f(-1, -1, 1);
  glTexCoord2f(1., 0.); glVertex3f( 1, -1, 1);
  glTexCoord2f(1., 1.); glVertex3f( 1,  1, 1);
  glTexCoord2f(0., 1.); glVertex3f(-1,  1, 1);
  // face droite
  glNormal3f(1, 0, 0);
  glTexCoord2f(0., 0.); glVertex3f( 1, -1,  1);
  glTexCoord2f(1., 0.); glVertex3f( 1, -1, -1);
  glTexCoord2f(1., 1.); glVertex3f( 1,  1, -1);
  glTexCoord2f(0., 1.); glVertex3f( 1,  1,  1);
  // face arrière
  glNormal3f(0, 0, -1);
  glTexCoord2f(1., 0.); glVertex3f(-1, -1,  -1);
  glTexCoord2f(1., 1.); glVertex3f(-1,  1,  -1);
  glTexCoord2f(0., 1.); glVertex3f( 1,  1,  -1);
  glTexCoord2f(0., 0.); glVertex3f( 1, -1,  -1);
  // face gauche
  glNormal3f(-1, 0, 0);
  glTexCoord2f(1., 0.); glVertex3f( -1, -1,  1);
  glTexCoord2f(1., 1.); glVertex3f( -1,  1,  1);
  glTexCoord2f(0., 1.); glVertex3f( -1,  1, -1);
  glTexCoord2f(0., 0.); glVertex3f( -1, -1, -1);
  // face dessus
  glNormal3f(0, 1, 0);
  glTexCoord2f(0., 0.); glVertex3f( -1,  1,  1);
  glTexCoord2f(1., 0.); glVertex3f(  1,  1,  1);
  glTexCoord2f(1., 1.); glVertex3f(  1,  1, -1);
  glTexCoord2f(0., 1.); glVertex3f( -1,  1, -1);
  // face dessous
  glNormal3f(0, -1, 0);
  glTexCoord2f(1., 0.); glVertex3f( -1,  -1,  1);
  glTexCoord2f(1., 1.); glVertex3f( -1,  -1, -1);
  glTexCoord2f(0., 1.); glVertex3f(  1,  -1, -1);
  glTexCoord2f(0., 0.); glVertex3f(  1,  -1,  1);
  glEnd();
}

void sol() {
  int nb_pas = 20;
  int tex_repeat = 8;

  glBindTexture(GL_TEXTURE_2D, tex[1]);
  glNormal3f(0, 1, 0);
  // Un sol dans le plan y = 0
  for (int i = 0; i < nb_pas; i++) {
    glBegin(GL_QUAD_STRIP);
    // On réalise une bande entre x = -10 et x = 10
    for (int j = 0; j <= nb_pas; j++) {
      glTexCoord2f((float) tex_repeat * j / nb_pas, (float) tex_repeat * i / nb_pas);
      glVertex3f(-10 + j * 20 / nb_pas,  0,  -10 + i * 20 / nb_pas);
      glTexCoord2f((float) tex_repeat * j / nb_pas, (float) tex_repeat * (i + 1) / nb_pas);
      glVertex3f(-10 + j * 20 / nb_pas,  0,  -10 + (i + 1) * 20 / nb_pas);
    }
    glEnd();
  }
}

void display() {
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  
  // Tous les matériaux partagent ici les mêmes propriétés
  GLfloat coefAmb[] = {.1, .1, .1, 1.};
  GLfloat coefDif[] = {.4, .4, .4, 1.};
  GLfloat coefSpe[] = {1., 1., 1., 1.};
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, coefAmb);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, coefDif);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, coefSpe);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50);

  glPushMatrix();
  glTranslatef(0, 3, 0);
  glEnable(GL_NORMALIZE);
  glScalef(2, 2, 2);
  cube();
  glDisable(GL_NORMALIZE);
  glPopMatrix();
  sol();

  SDL_GL_SwapWindow(fenetre);

  if (glGetError() != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans display\n");
  }
}

void init_SDL() {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Couldn't initialize video driver: %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    
  fenetre = SDL_CreateWindow("B-A-BA textures",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

int main(void) {
  init_SDL();
  initGL();
  initTexture();

  int continuer = 1;
  SDL_Event event;
  while (continuer) {
      SDL_PollEvent(&event);
      switch(event.type) {
      case SDL_QUIT:
        continuer = 0;
        break;
      case SDL_KEYDOWN:
        switch(event.key.keysym.sym) {
        case SDLK_ESCAPE:
          continuer=0;
          break;
        default:
          break;
        }
      default:
        break;
      }
      display();
  }
  
  SDL_Quit();
  
  return 0;
}

