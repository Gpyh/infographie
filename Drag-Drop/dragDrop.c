#include <stdbool.h>
#include <math.h>

#include <SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifndef M_PI
#define M_PI 3.141592654
#endif

#define degRad(a) (((double) a) * M_PI / 180.)

/* Taille du tampon de s�lection */
#define BUFSIZE 512

/* Position de l'observateur */
float rho = 15;
float theta = 90;
float phi = 0;

/* Gestion de la souris */
int boutonGaucheAppuye = false;
int boutonDroitAppuye = false;

/* Position vis�e */
double x_p, y_p, z_p;

/* position vis�e dans la theiere */
double vise_x, vise_y, vise_z;

int bougeTheiere = false;

/* Lumi�re (fixe par rapport � la sc�ne) */
GLfloat lumPosition[4] = {3., 3., 0., 1.};
GLfloat lumAmbiante[4] = {1., 1., 1., 1.};
GLfloat lumAmbianteRouge[4] = {1., 0., 0., 1.};
GLfloat lumDiffuse[4] = {1., 1., 1., 1.};
GLfloat lumSpeculaire[4] = {1., 1., 1., 1.};
GLfloat lumZero[4] = {0., 0., 0., 1.};
GLfloat lumPeuDiffuse[4];

/* Une quadrique pour la GLU */
GLUquadric * quad;

SDL_Window *fenetre;
SDL_GLContext context;

GLenum erreur;

/**
 * R�alise un gluLookAt en fonction de la postion
 * (rho, theta, phi) de l'observateur.
 */
void positionneObservateur() {
  double posObsx, posObsy, posObsz;
  double basHautx, basHauty, basHautz;

  /* Calcul de la nouvelle position de l'observateur */
  posObsx = rho * cos(degRad(phi)) * cos(degRad(theta));
  posObsy = rho * sin(degRad(phi));
  posObsz = rho * cos(degRad(phi)) * sin(degRad(theta));

  /* Calcul du nouveau vecteur bas-haut */
  basHautx = sin(degRad(-phi)) * cos(degRad(theta));
  basHauty = cos(degRad(-phi));
  basHautz = sin(degRad(-phi)) * sin(degRad(theta));

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(posObsx, posObsy, posObsz, 0, 0, 0, basHautx, basHauty, basHautz);
}

void reshape(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, (GLdouble) w / h, 8.5, 50);
  
  glMatrixMode(GL_MODELVIEW);
}

void init_SDL() {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Erreur � l'initialisation de la vid�o : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  
  fenetre = SDL_CreateWindow("B-A-BA Drag and Drop",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la cr�ation de la fen�tre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la cr�ation du contexte OpenGL\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
}

void init_GL(void) {
  glEnable(GL_DEPTH_TEST);
  
  glClearColor(0., 0., 0., 0.);
  
  glEnable(GL_LIGHTING);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lumZero);
  
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_AMBIENT, lumAmbiante);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lumDiffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lumSpeculaire);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, 1, 8.5, 50);
  glMatrixMode(GL_MODELVIEW);

  positionneObservateur();
  
  glPointSize(5);

  quad = gluNewQuadric();
}

void GL_Quit() {
  gluDeleteQuadric(quad);
}

/**
 * Affichage de la scene selon deux modes :
 * <ul>
 * <li>GL_RENDER affiche la sc�ne</li>
 * <li>GL_SELECT s�lectionne les objets pr�sents dans le volume de vue/li>
 * </ul>
 */
void dessineScene(GLenum mode) {
  if (mode == GL_RENDER) {
    glEnable(GL_LIGHTING);
    glLightfv(GL_LIGHT0, GL_POSITION, lumPosition);
  }
  else {
    glDisable(GL_LIGHTING);
  }
  
  /* Sol */
  if (mode == GL_SELECT) {
    // On associe le nom "1" au sol
    glLoadName(1);
  }
  glBegin(GL_QUADS);
  glNormal3f(0, 1, 0);    
  glVertex3f(-3., -1.2, 3.);
  glVertex3f(3., -1.2, 3.);
  glVertex3f(3., -1.2, -3.);
  glVertex3f(-3., -1.2, -3.);
  glEnd();
  
  glPushMatrix();
  if (bougeTheiere) {
    /* On est en train de d�placer l'objet.
     * Le vecteur de translation correspond au vecteur
     * position de la souris dans le monde virtuel
     *   - position vis�e au d�but du d�placement
     */
    glTranslated(x_p - vise_x, y_p - vise_y, z_p - vise_z);
  }
  if (mode == GL_SELECT) {
    // On associe le nom "2" � la th�i�re
    glLoadName(2);
  }
  //glutSolidTeapot(.3);
  glTranslatef(0., -.7, 0.);
  gluSphere(quad, .5, 10, 10);
  glPopMatrix();
  
  if (boutonDroitAppuye && mode == GL_RENDER) {
    /* On affiche un point � l'endroit s�lectionn� */
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glColor3f(1, 0, 0);
    glBegin(GL_POINTS);
    glVertex3f(x_p, y_p, z_p);
    glEnd();
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
  }
}


void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  dessineScene(GL_RENDER);
  
  SDL_GL_SwapWindow(fenetre);

  if ((erreur = glGetError()) != GL_NO_ERROR) {
    printf("Erreur � la fin de l'affichage\n");
  }
}

/**
 * Fonction appel�e lorsqu'on d�place la th�i�re.
 * @param x l'abscisse de la position de la souris selon
 * les coordonn�es �cran.
 * @param y l'ordonn�e de la position de la souris selon
 * les coordonn�es �cran.
 */
void traiteDragDrop(int x, int y) {
  GLdouble x_near, y_near, z_near;
  const GLdouble modelMatrix[16];
  const GLdouble projMatrix[16];
  const GLint viewport[4];
  double posObsx, posObsy, posObsz;
  double t;
  
  /* On commence par convertir les coordonn�es de la souris en
   * coordonn�es du rep�re universel dans le plan de projection.
   */ 
  glGetDoublev(GL_MODELVIEW_MATRIX, (void *) modelMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX, (void *) projMatrix);
  glGetIntegerv(GL_VIEWPORT, (void *) viewport);
  /* En coordonn�es �cran l'ordonn�e 0 est en haut et elle augmente quand on descend,
   * alors que dans le rep�re universel c'est le contraire : l'axe des ordonn�es est dirig� vers le haut.
   * Il faut donc "inverser" le y.
   */
  y = viewport[3] -y;
  gluUnProject(x, y, 0,
               modelMatrix, projMatrix, viewport,
               &x_near, &y_near, &z_near);

  /* Calcul de la position de l'observateur dans le rep�re universel */
  posObsx = rho * cos(degRad(phi)) * cos(degRad(theta));
  posObsy = rho * sin(degRad(phi));
  posObsz = rho * cos(degRad(phi)) * sin(degRad(theta));

  /* Calcul de l'intersection plan de la hauteur theiere - droite de projection.
   * L'�quation de la droite de projection est : P = t * P_Obs + (1 - t) * P_near.
   * Celle du plan contenant la th�i�re : y = vise_y.
   * On peut en d�duire l'intersection en r�solvant le syst�me d'�quation.
   */
  t = (vise_y - y_near) / (posObsy - y_near);
  x_p = t * posObsx + (1 - t) * x_near;
  y_p = vise_y;
  z_p = t * posObsz + (1 - t) * z_near;

  /* Si la th�i�re arrive en dehors du quadrilat�re repr�sentant le sol,
   * on la remet dedans.
   */
  if (x_p < -3) {
    x_p = -3;
  } else if (x_p > 3) {
    x_p = 3;
  } if (z_p < -3) {
    z_p = -3;
  } else if (z_p > 3) {
    z_p = 3;
  }
}

/**
 * Fonction appel�e lorsqu'on bouge la souris.
 */
void motion(Sint32 x, Sint32 y, Sint32 xrel, Sint32 yrel) {
  if (boutonGaucheAppuye) {
    theta += xrel;
    phi += yrel;
    positionneObservateur();
  } else if (boutonDroitAppuye) {
    traiteDragDrop(x, y);
  }
}

/**
 * Traitement des informations re�ues d'OpenGL lors du "picking".
 * @param hits nombre d'objets qui ont �t� s�lectionn�es.
 * @param buffer tableau contenant les informations concernant les objets.
 */
double processHits(GLint hits, GLuint buffer[]) {
  unsigned int  j;
  int i;
  GLuint names, *ptr;
  GLfloat z1, /* z2, */z_theiere;

  // Valeur par d�faut indiquant que la th�i�re n'a pas �t� s�lectionn�e
  z_theiere = -100;

  // Parcours du tableau
  ptr = (GLuint *) buffer ;
  for (i = 0; i < hits; i++) {
    // On r�cup�re le nombre de noms associ�s � l'objet
    names = *ptr;
    ptr++;

    // On r�cup�re l'extension en z de l'objet
    // (z min et z max)
    // z est une valeur comprise entre [0, 1] multipli�e par 2^32 - 1
    z1 = (GLfloat) (*ptr) / ((float) 4294967295);
    ptr++;
    // z2 = (GLfloat) (*ptr) / ((float) 4294967295);
    ptr++;

    // On r�cup�re les noms associ�s � l'objet
    for (j = 0 ; j < names; j++) {
      if ((*ptr) == 2) {
        // Le nom "2" indique une face de la th�i�re
        // On m�morise le z minimal
        z_theiere = z1;
      }
      ptr++;
    }
  }
  
  return z_theiere;
}

/**
 * Calcul du point de la th�i�re qui a �t� s�lectionn� par la souris.
 */
void calculPointVise(int x, int y) {
  GLuint selectBuf[BUFSIZE];
  GLint hits;
  GLint viewport[4];

  double z_theiere;
  
  // On r�cup�re les dimensions du viewport
  glGetIntegerv(GL_VIEWPORT,viewport);

  // On indique le tableau devant recevoir les propri�t�s des objets s�lectionn�s
  glSelectBuffer(BUFSIZE, selectBuf);

  // On place la machine OpenGL en mode de s�lection
  glRenderMode(GL_SELECT);
  // On initialise la pile des noms
  glInitNames();
  glPushName(0);

  // On d�finit un volume de projection restreint aux pixels sous la souris
  // (et un peu � c�t�).
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3] - y), 5.0, 5.0, viewport) ;
  gluPerspective(30, (GLdouble) viewport[2] / viewport[3], 8.5, 50);

  // On calcule la sc�ne pour r�cup�rer les objets qui sont sous la souris
  glMatrixMode(GL_MODELVIEW);
  dessineScene(GL_SELECT);

  // On restore la projection
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  // On analyse les informations transmises par OpenGL
  hits = glRenderMode(GL_RENDER);
  z_theiere = processHits(hits, selectBuf);

  // Si la th�i�re est s�lectionn�e, on calcule le point vis� dans la th�i�re
  if (z_theiere != -100) {
    GLdouble modelMatrix[16];
    GLdouble projMatrix[16];
    GLint viewport[4];
    
    // On demande � la GLU de calculer le point dans l'univers qui correspond
    // � la position �cran de la souris et qui a un z correspondant � celui
    // de l'objet s�lectionn�.
    glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
    glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);
    glGetIntegerv(GL_VIEWPORT, viewport);
    gluUnProject(x, viewport[3] - y, z_theiere,
                 modelMatrix, projMatrix, viewport,
                 &vise_x, &vise_y, &vise_z);

    // On m�morise �galement le point vis� dans les variables indiquant le
    // d�placement de la souris dans l'univers.
    x_p = vise_x;
    y_p = vise_y;
    z_p = vise_z;
    
    // On indique qu'il faudra bouger la th�i�re si la souris se d�place
    bougeTheiere = true;
  } else {
    // Le sol est dans le plan y = -1.2
    vise_y = -1.2;
    vise_x = vise_z = 0;
    y_p = -1.2;
    x_p = z_p = 0;
  }
}

/**
 * Fonction appel�e lorsqu'on appuie sur un bouton de la souris.
 */
void mouseButton(SDL_MouseButtonEvent button) {
  if (button.button == SDL_BUTTON_LEFT && button.state == SDL_PRESSED) {
    // On va d�placer l'observateur
    boutonGaucheAppuye = true;
  } else if (button.button == SDL_BUTTON_LEFT && button.state == SDL_RELEASED) {
    boutonGaucheAppuye = false;
  } else if (button.button == SDL_BUTTON_RIGHT && button.state == SDL_PRESSED) {
    // On va d�placer la th�i�re
    boutonDroitAppuye = true;
    calculPointVise(button.x, button.y);
  } else if (button.button == SDL_BUTTON_RIGHT && button.state == SDL_RELEASED) {
    boutonDroitAppuye = false;
    bougeTheiere = false;
  }
}

int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    default:
      break;
    }
  }
  
  return 1;
}

int main(void) {
  init_SDL();
  init_GL();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les �v�nements de la fenetre
  while (continuer) {
    // On attend le prochain �v�nement
    SDL_WaitEvent(&event);
    // On traite l'�v�nement
    switch (event.type) {
    case SDL_QUIT:
      // On a ferm� la fenetre
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      // On a appuy� sur une touche
      continuer = keyboard(&event);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_RESIZED:
        reshape(event.window.data1, event.window.data2);
        break;
      }
      break;
    case SDL_MOUSEMOTION:
      {
        SDL_Event event_temp;
        int depx = event.motion.xrel;
        int depy = event.motion.yrel;
        while (SDL_PeepEvents(&event_temp, 1, SDL_PEEKEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION) > 0) {
          SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION);
          depx += event.motion.xrel;
          depy += event.motion.yrel;
        }
        motion(event.motion.x, event.motion.y, depx, depy);
        break;
      }
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
      mouseButton(event.button);
      break;
    }
    display();
  }
  
  // C'est fini : on lib�re les ressources proprement
  GL_Quit();
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();
  
  return EXIT_SUCCESS;
}
