/**
 * Petit programme d'exemple d'utilisation des VBO (vertex buffer object).
 * Utilise des fonctions introduites dans OpenGL 1.5.
 */

#include <stdlib.h>
#include <stdio.h>

#include <GL/glew.h>
#include <GL/glu.h>

#include <SDL.h>

SDL_Window *fenetre;
SDL_GLContext context;

void init_SDL(void);
void init_GL(void);
void init_VBO(void);
void display(void);
int keyboard(SDL_Event * event);

int main(int argc, char **argv) {
  if (argc != 1) {
    fprintf(stderr, "Usage: %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  init_SDL();
  init_GL();
  init_VBO();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les évènements de la fenetre
  while (continuer) {
    // On attend le prochain évènement
    SDL_WaitEvent(&event);
    // On traite l'évènement
    switch (event.type) {
    case SDL_QUIT:
      // On a fermé la fenetre
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      // On a appuyé sur une touche
      continuer = keyboard(&event);
      break;
    }
    display();
  }
  
  // C'est fini : on libère les ressources proprement
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();
  
  return EXIT_SUCCESS;
}

/**
 * Initialisation de la SDL, création du contexte OpenGL et ouverture de la fenêtre.
 */
void init_SDL(void) {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Couldn't initialize video driver: %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    
  fenetre = SDL_CreateWindow("B-A-BA Buffer Object",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

/**
 * Initialisation des états du contexte OpenGL.
 */
void init_GL(void) {
  GLenum err = glewInit();
  if (err != GLEW_OK) {
    fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    exit(EXIT_FAILURE);
  }
  if (!glewIsSupported("GL_VERSION_1_5")) {
    fprintf(stderr, "Error: OpenGL 1.5 not supported\n");
    exit(EXIT_FAILURE);
  }

  glClearColor(1., 1., 1., 0.);
  
  glMatrixMode(GL_PROJECTION);
  gluPerspective(60, 1, 1, 10);
  glMatrixMode(GL_MODELVIEW);
  gluLookAt(2, 2, 2, 0, 0, 0, -1, 1, -1);

  glEnable(GL_DEPTH_TEST);
}

/**
 * Initialisation des VBO
 */
GLuint indicesElements;

void init_VBO(void) {

  GLfloat vertices[] = {
            -0.5f, -0.5f, -0.5f, 1.0f, // face y = -0.5
            -0.5f, -0.5f, +0.5f, 1.0f,
            +0.5f, -0.5f, +0.5f, 1.0f,
            +0.5f, -0.5f, -0.5f, 1.0f,
            -0.5f, +0.5f, -0.5f, 1.0f, // face y = +0.5
            -0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, -0.5f, 1.0f,
            -0.5f, -0.5f, -0.5f, 1.0f, // face z = -0.5
            -0.5f, +0.5f, -0.5f, 1.0f,
            +0.5f, +0.5f, -0.5f, 1.0f,
            +0.5f, -0.5f, -0.5f, 1.0f,
            -0.5f, -0.5f, +0.5f, 1.0f, // face z = +0.5
            -0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, -0.5f, +0.5f, 1.0f,
            -0.5f, -0.5f, -0.5f, 1.0f, // face x = -0.5
            -0.5f, -0.5f, +0.5f, 1.0f,
            -0.5f, +0.5f, +0.5f, 1.0f,
            -0.5f, +0.5f, -0.5f, 1.0f,
            +0.5f, -0.5f, -0.5f, 1.0f, // face x = +0.5
            +0.5f, -0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, -0.5f, 1.0f
    
  };

  // Les couleurs des sommets
  GLfloat colors[] = {
            +1.0f, +0.0f, +0.0f, 1.0f, // face y = -0.5, rouge
            +1.0f, +0.0f, +0.0f, 1.0f,
            +1.0f, +0.0f, +0.0f, 1.0f,
            +1.0f, +0.0f, +0.0f, 1.0f,
            +0.0f, +1.0f, +0.0f, 1.0f, // face y = +0.5, verte
            +0.0f, +1.0f, +0.0f, 1.0f,
            +0.0f, +1.0f, +0.0f, 1.0f,
            +0.0f, +1.0f, +0.0f, 1.0f,
            +0.0f, +0.0f, +1.0f, 1.0f, // face z = -0.5, bleue
            +0.0f, +0.0f, +1.0f, 1.0f,
            +0.0f, +0.0f, +1.0f, 1.0f,
            +0.0f, +0.0f, +1.0f, 1.0f,
            +1.0f, +1.0f, +0.0f, 1.0f, // face z = +0.5, jaune
            +1.0f, +1.0f, +0.0f, 1.0f,
            +1.0f, +1.0f, +0.0f, 1.0f,
            +1.0f, +1.0f, +0.0f, 1.0f,
            +1.0f, +0.0f, +1.0f, 1.0f, // face x = -0.5, violette
            +1.0f, +0.0f, +1.0f, 1.0f,
            +1.0f, +0.0f, +1.0f, 1.0f,
            +1.0f, +0.0f, +1.0f, 1.0f,
            +0.0f, +1.0f, +1.0f, 1.0f, // face x = +0.5, cyan
            +0.0f, +1.0f, +1.0f, 1.0f,
            +0.0f, +1.0f, +1.0f, 1.0f,
            +0.0f, +1.0f, +1.0f, 1.0f
    };

  // Les normales des sommets
  GLfloat normals[] = {
            +0.0f, -1.0f, +0.0f, // face y = -0.5
            +0.0f, -1.0f, +0.0f, // n = (0, -1, 0)
            +0.0f, -1.0f, +0.0f,
            +0.0f, -1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f, // face y = +0.5
            +0.0f, +1.0f, +0.0f, // n = (0, +1, 0)
            +0.0f, +1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f,
            +0.0f, +0.0f, -1.0f, // face z = -0.5
            +0.0f, +0.0f, -1.0f, // n = (0, 0, -1)
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, +1.0f, // face z = +0.5
            +0.0f, +0.0f, +1.0f, // n = (0, 0, +1)
            +0.0f, +0.0f, +1.0f,
            +0.0f, +0.0f, +1.0f,
            -1.0f, +0.0f, +0.0f, // face x = -0.5
            -1.0f, +0.0f, +0.0f, // n = (-1, 0, 0)
            -1.0f, +0.0f, +0.0f,
            -1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f, // face x = +0.5
            +1.0f, +0.0f, +0.0f, // n = (+1, 0, 0)
            +1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f
    };

  GLuint vertexAttrib[3];
  glGenBuffers(3, vertexAttrib);
  
  // Les sommets
  glBindBuffer(GL_ARRAY_BUFFER, vertexAttrib[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 24 * 4, vertices, GL_STATIC_DRAW);
  glVertexPointer(4, GL_FLOAT, 0, 0);

  // Les couleurs
  glBindBuffer(GL_ARRAY_BUFFER, vertexAttrib[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 24 * 4, colors, GL_STATIC_DRAW);
  glColorPointer(4, GL_FLOAT, 0, 0);

  // Les normales
  glBindBuffer(GL_ARRAY_BUFFER, vertexAttrib[2]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 24 * 3, normals, GL_STATIC_DRAW);
  glNormalPointer(GL_FLOAT, 0, 0);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);

  /* Les indices des sommets du cube */
  GLuint indices[] = {
            0,  2,  1, // face y = -0.5
            0,  3,  2,
            4,  5,  6, // face y = +0.5
            4,  6,  7,
            8,  9, 10, // face z = -0.5
            8, 10, 11,
            12, 15, 14, // face z = +0.5
            12, 14, 13,
            16, 17, 18, // face x = -0.5
            16, 18, 19,
            20, 23, 22, // face x = +0.5
            20, 22, 21
    };

  glGenBuffers(1, &indicesElements);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesElements);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * 6 * 2 * 3, indices, GL_STATIC_DRAW);

  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans init_VBO : %s\n", gluErrorString(erreur));
  }

}

/**
 * Affichage de la scène OpenGL.
 */
void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesElements);
  glDrawElements(GL_TRIANGLES, 6 * 2 * 3, GL_UNSIGNED_INT, NULL);

  SDL_GL_SwapWindow(fenetre);

  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans display : %s\n", gluErrorString(erreur));
  }
}

/**
 * Gestion du clavier.
 * @param event l'évènement SDL ayant déclenché l'appel à la fonction
 * (doit etre de type SDL_KEYDOWN).
 */
int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    default:
      break;
    }
  }
  
  return 1;
}

