/**
 * Petit programme d'exemple de déplacement d'une caméra.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <GL/gl.h>

#include <SDL.h>
#include <GL/glu.h>

#include "camera.h"

SDL_Window *fenetre;
SDL_GLContext context;

typedef enum {SUBJECTIVE, TROISIEME_PERSONNE} enum_mode_vue;
enum_mode_vue mode_vue = SUBJECTIVE;

/**
 * Contient l'état des "fleches" du clavier.
 * Au début, aucune touche n'est dans l'état appuyé.
 * Tous les champs sont à 0 et donc à SDL_FALSE.
 */
struct {
    int up;
    int down;
    int left;
    int right;
} keyboard_state;

// Contient le timestamp du dernier affichage
Uint32 timestamp;

int video_flags;

#define NEAR_SUBJECT (.4)
#define FAR_SUBJECT (20)
#define NEAR_TP (10)
#define FAR_TP (100)

int win_width = 640;
int win_height = 640;

void reshape(int w, int h);
void init_SDL(void);
void init_GL(void);
void setView(void);
void reshape(int w, int h);
void display(void);
void positionneObservateur(void);
int keyboard(SDL_Event * event);
void motion(int depx, int depy);
void cube();

int main(void) {
    init_SDL();
    init_GL();
    
    int continuer = 1;
    SDL_Event event;
    
    printf("Appuyer sur [Entrée] pour changer le mode de vue\n");
    printf("Utiliser les flèches pour vous déplacer\n");
    printf("La souris sert à orienter la caméra\n");
    
    // Boucle traitant les évènements de la fenêtre
    while (continuer) {
        // On traite le prochain évènement
        if (SDL_PollEvent(&event)) {
            // On traite l'évènement
            switch (event.type) {
            case SDL_QUIT:
                // On a fermé la fenetre
                continuer = 0;
                break;
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                // On a appuyé ou relaché une touche
                continuer = keyboard(&event);
                break;
            case SDL_MOUSEMOTION:
              {
                SDL_Event event_temp;
                int depx = event.motion.xrel;
                int depy = event.motion.yrel;
                while (SDL_PeepEvents(&event_temp, 1, SDL_PEEKEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION) > 0) {
                  SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION);
                  depx += event.motion.xrel;
                  depy += event.motion.yrel;
                }
                motion(depx, depy);
                break;
              }
            case SDL_WINDOWEVENT:
              switch (event.window.event) {
              case SDL_WINDOWEVENT_RESIZED:
                reshape(event.window.data1, event.window.data2);
                break;
              }
              break;
            }
        }
        positionneObservateur();
        display();
    }
    
    // C'est fini : on libère les ressources proprement
    SDL_SetWindowFullscreen(fenetre, 0);
    SDL_Quit();
    
    return EXIT_SUCCESS;
}

/**
 * Initialisation de la SDL, création du contexte OpenGL et ouverture de la fenetre.
 */
void init_SDL(void) {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Couldn't initialize video driver: %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  // Multisampling anti-aliasing
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  
  fenetre = SDL_CreateWindow("SDL camera fluide",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }

  SDL_GL_SetSwapInterval(1);

  if (SDL_SetWindowFullscreen(fenetre, SDL_WINDOW_FULLSCREEN_DESKTOP) != 0) {
    fprintf(stderr, "Erreur lors du passage au plein au écran : %s\n",
            SDL_GetError());
  }

  SDL_ShowCursor(SDL_DISABLE);
  SDL_SetWindowGrab(fenetre, SDL_TRUE);
  
  if (SDL_SetRelativeMouseMode(SDL_TRUE) == -1) {
    fprintf(stderr, "Le mode relatif pour la souris n'est pas supporté\n");
  }
    
}

/**
 * Fixe la projection en fonction du
 * mode de vue (subjectif ou troisième personne).
 */
void setView(void) {
    if (mode_vue == SUBJECTIVE) {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(60, (float) win_width / win_height, NEAR_SUBJECT, FAR_SUBJECT);
        glMatrixMode(GL_MODELVIEW);
    } else {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(50, (float) win_width / win_height, NEAR_TP, FAR_TP);
        glMatrixMode(GL_MODELVIEW);
    }
}

camCamera camera;

/**
 * Initialisation des états du contexte OpenGL.
 */
void init_GL(void) {
    glClearColor(1., 1., 1., 0.);
    
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);

    camInit(&camera);
    camFixePosition(&camera, 0, .5, 3, 0, 0, 0, 0, 1, 0);
    if (mode_vue == SUBJECTIVE) {
        camLookAt(&camera);
    } else {
        gluLookAt(0, 4, 13, 0, 0, 0, 0, 1, 0);
    }
    setView();
}

void reshape(int w, int h) {
    // On redéfinit le viewport
    glViewport(0, 0, w, h);
    if (mode_vue == SUBJECTIVE) {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(60, (float) w / h, NEAR_SUBJECT, FAR_SUBJECT);
        glMatrixMode(GL_MODELVIEW);
    } else {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(50, (float) w / h, NEAR_TP, FAR_TP);
        glMatrixMode(GL_MODELVIEW);
    }
    win_width = w;
    win_height = h;
}

/**
 * Affichage de la scène OpenGL.
 */
void display(void) {
  timestamp = SDL_GetTicks();
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
  // Le sol
  glBegin(GL_TRIANGLES);
    glColor3f(.5, .5, .5);
    glVertex3f(-10, -.5,  10);
    glVertex3f( 10, -.5,  10);
    glVertex3f(-10, -.5, -10);

    glVertex3f(-10, -.5, -10);
    glVertex3f( 10, -.5, 10);
    glVertex3f( 10, -.5, -10);
  glEnd();
    
  // Le repère de l'observateur
  camVector vecObsx;
  vecObsx.x = - camera.up.y * camera.forward.z + camera.up.z * camera.forward.y;
  vecObsx.y = - camera.up.z * camera.forward.x + camera.up.x * camera.forward.z;
  vecObsx.z = - camera.up.x * camera.forward.y + camera.up.y * camera.forward.x;
  
  glBegin(GL_LINES);
    glColor3f(0, 1, 0);
    glVertex3f(camera.eye.x, camera.eye.y, camera.eye.z);
    glVertex3f(camera.eye.x + vecObsx.x, camera.eye.y + vecObsx.y, camera.eye.z + vecObsx.z);
    glColor3f(1, 0, 0);
    glVertex3f(camera.eye.x, camera.eye.y, camera.eye.z);
    glVertex3f(camera.eye.x - camera.forward.x, camera.eye.y - camera.forward.y, camera.eye.z - camera.forward.z);
    glColor3f(0, 0, 1);
    glVertex3f(camera.eye.x, camera.eye.y, camera.eye.z);
    glVertex3f(camera.eye.x + camera.up.x, camera.eye.y + camera.up.y, camera.eye.z + camera.up.z);
  glEnd();
    
  // Un cube
  cube();
  
  SDL_GL_SwapWindow(fenetre);
  
  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans display : %s\n", gluErrorString(erreur));
  }
  
}

/**
 * Gestion du déplacement de l'observateur en fonction de l'état
 * des touches du clavier.
 */
void positionneObservateur(void) {
    /**
     * Normalement il faudrait calculer la norme du vecteur déplacement
     * en fonction de la vitesse de déplacement de l'observateur.
     * Ici on applique un vecteur déplacement si l'utisateur avance et un 
     * autre s'il fait un pas de côté : il se déplacera donc plus vite
     * s'il avance et fait un pas de côté que s'il avance seulement/
     */
    float vitesse = 0.01;
    float deplacement = vitesse * (SDL_GetTicks() - timestamp);

    if (keyboard_state.up) {
        camAvance(&camera, deplacement);        
    }
    if (keyboard_state.down) {
        camAvance(&camera, -deplacement);        
    }
    if (keyboard_state.left) {
        camPasDeCote(&camera, -deplacement);        
    }
    if (keyboard_state.right) {
        camPasDeCote(&camera, deplacement);        
    }

    glLoadIdentity();
    if (mode_vue == SUBJECTIVE) {
        camLookAt(&camera);
    } else {
        gluLookAt(0, 4, 13, 0, 0, 0, 0, 1, 0);
    }
}

/**
 * Gestion du clavier.
 * @param event l'évènement SDL ayant déclenché l'appel à la fonction
 * (doit etre de type SDL_KEYDOWN ou SDL_KEYDOWN).
 */
int keyboard(SDL_Event * event) {
    switch(event->key.keysym.sym) {
    case SDLK_UP:
        keyboard_state.up = event->type == SDL_KEYDOWN;
        break;
    case SDLK_DOWN:
        keyboard_state.down = event->type == SDL_KEYDOWN;
        break;
    case SDLK_LEFT:
        keyboard_state.left = event->type == SDL_KEYDOWN;
        break;
    case SDLK_RIGHT:
        keyboard_state.right = event->type == SDL_KEYDOWN;
        break;
    default:
        break;
    }
    
    if (event->type == SDL_KEYDOWN) {
        switch(event->key.keysym.sym) {
        case SDLK_ESCAPE:
            return 0;
        case SDLK_RETURN:
            if (mode_vue == SUBJECTIVE) {
                mode_vue = TROISIEME_PERSONNE;
            } else {
                mode_vue = SUBJECTIVE;
            }
            setView();
            break;
        default:
            break;
        }
    }
    
    return 1;
}

int doMotion = 0;

void motion(int depx, int depy) {
    if (doMotion) {
        // Sert à ramener le déplacement dans des intervalles corrects pour
        // camTourneEtLeve()
        float normalisation = 0.2;
    
        camTourneEtLeve(&camera,  normalisation * depx, normalisation * depy);
    
        if (mode_vue == SUBJECTIVE) {
            camLookAt(&camera);
        }
    } else {
        // La première fois xrel et yrel ne sont pas pertinents,
        // aussi on ne déplace pas la caméra
        doMotion = 1;
    }
}

/**
 * Dessine un cube centré autour de O, dont toutes les faces ont des couleurs
 * différentes et dont les côtés sont de longueurs 2.
 */
void cube(void) {
  // On va utiliser des indexed vertex arrays pour afficher notre cube.
  // Nécessite OpengGL 1.1
  
  GLfloat vertices[] = {
            -0.5f, -0.5f, -0.5f, 1.0f, // face y = -0.5
            -0.5f, -0.5f, +0.5f, 1.0f,
            +0.5f, -0.5f, +0.5f, 1.0f,
            +0.5f, -0.5f, -0.5f, 1.0f,
            -0.5f, +0.5f, -0.5f, 1.0f, // face y = +0.5
            -0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, -0.5f, 1.0f,
            -0.5f, -0.5f, -0.5f, 1.0f, // face z = -0.5
            -0.5f, +0.5f, -0.5f, 1.0f,
            +0.5f, +0.5f, -0.5f, 1.0f,
            +0.5f, -0.5f, -0.5f, 1.0f,
            -0.5f, -0.5f, +0.5f, 1.0f, // face z = +0.5
            -0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, -0.5f, +0.5f, 1.0f,
            -0.5f, -0.5f, -0.5f, 1.0f, // face x = -0.5
            -0.5f, -0.5f, +0.5f, 1.0f,
            -0.5f, +0.5f, +0.5f, 1.0f,
            -0.5f, +0.5f, -0.5f, 1.0f,
            +0.5f, -0.5f, -0.5f, 1.0f, // face x = +0.5
            +0.5f, -0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, +0.5f, 1.0f,
            +0.5f, +0.5f, -0.5f, 1.0f
    
  };

  // Les couleurs des sommets
  GLfloat colors[] = {
            +1.0f, +0.0f, +0.0f, 1.0f, // face y = -0.5, rouge
            +1.0f, +0.0f, +0.0f, 1.0f,
            +1.0f, +0.0f, +0.0f, 1.0f,
            +1.0f, +0.0f, +0.0f, 1.0f,
            +0.0f, +1.0f, +0.0f, 1.0f, // face y = +0.5, verte
            +0.0f, +1.0f, +0.0f, 1.0f,
            +0.0f, +1.0f, +0.0f, 1.0f,
            +0.0f, +1.0f, +0.0f, 1.0f,
            +0.0f, +0.0f, +1.0f, 1.0f, // face z = -0.5, bleue
            +0.0f, +0.0f, +1.0f, 1.0f,
            +0.0f, +0.0f, +1.0f, 1.0f,
            +0.0f, +0.0f, +1.0f, 1.0f,
            +1.0f, +1.0f, +0.0f, 1.0f, // face z = +0.5, jaune
            +1.0f, +1.0f, +0.0f, 1.0f,
            +1.0f, +1.0f, +0.0f, 1.0f,
            +1.0f, +1.0f, +0.0f, 1.0f,
            +1.0f, +0.0f, +1.0f, 1.0f, // face x = -0.5, violette
            +1.0f, +0.0f, +1.0f, 1.0f,
            +1.0f, +0.0f, +1.0f, 1.0f,
            +1.0f, +0.0f, +1.0f, 1.0f,
            +0.0f, +1.0f, +1.0f, 1.0f, // face x = +0.5, cyan
            +0.0f, +1.0f, +1.0f, 1.0f,
            +0.0f, +1.0f, +1.0f, 1.0f,
            +0.0f, +1.0f, +1.0f, 1.0f
    };

  // Les normales des sommets
  GLfloat normals[] = {
            +0.0f, -1.0f, +0.0f, // face y = -0.5
            +0.0f, -1.0f, +0.0f, // n = (0, -1, 0)
            +0.0f, -1.0f, +0.0f,
            +0.0f, -1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f, // face y = +0.5
            +0.0f, +1.0f, +0.0f, // n = (0, +1, 0)
            +0.0f, +1.0f, +0.0f,
            +0.0f, +1.0f, +0.0f,
            +0.0f, +0.0f, -1.0f, // face z = -0.5
            +0.0f, +0.0f, -1.0f, // n = (0, 0, -1)
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, -1.0f,
            +0.0f, +0.0f, +1.0f, // face z = +0.5
            +0.0f, +0.0f, +1.0f, // n = (0, 0, +1)
            +0.0f, +0.0f, +1.0f,
            +0.0f, +0.0f, +1.0f,
            -1.0f, +0.0f, +0.0f, // face x = -0.5
            -1.0f, +0.0f, +0.0f, // n = (-1, 0, 0)
            -1.0f, +0.0f, +0.0f,
            -1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f, // face x = +0.5
            +1.0f, +0.0f, +0.0f, // n = (+1, 0, 0)
            +1.0f, +0.0f, +0.0f,
            +1.0f, +0.0f, +0.0f
    };
  
  /* Les indices des sommets du cube */
  GLuint indexes[] = {
            0,  2,  1, // face y = -0.5
            0,  3,  2,
            4,  5,  6, // face y = +0.5
            4,  6,  7,
            8,  9, 10, // face z = -0.5
            8, 10, 11,
            12, 15, 14, // face z = +0.5
            12, 14, 13,
            16, 17, 18, // face x = -0.5
            16, 18, 19,
            20, 23, 22, // face x = +0.5
            20, 22, 21
    };

  glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
  
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(4, GL_FLOAT, 0, vertices);

  glEnableClientState(GL_COLOR_ARRAY);
  glColorPointer(4, GL_FLOAT, 0, colors);

  glEnableClientState(GL_NORMAL_ARRAY);
  glNormalPointer(GL_FLOAT, 0, normals);
 
  glDrawElements(GL_TRIANGLES, 6 * 2 * 3, GL_UNSIGNED_INT, indexes);

  glPopClientAttrib();
}
