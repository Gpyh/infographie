/**
 * Exemple d'utilisation de SDL2_ttf pour afficher du texte avec
 * OpenGL.
 * Il s'agit d'un exemple minimal qui mériterait d'être factorisé.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glu.h>
#include <GL/gl.h>

SDL_Window *fenetre;
SDL_GLContext context;

// On n'utilise dans notre exemple qu'une seule fonte
// qu'on charge à l'initialisation
TTF_Font *font;

void init_SDL(void);
void init_TTF(void);
void init_GL(void);
void display(void);
int keyboard(SDL_Event * event);
void reshape(int w, int h);

bool fullscreen = false;

int main(int argc, char **argv) {
  if (argc != 1) {
    fprintf(stderr, "Usage: %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  init_SDL();
  init_TTF();
  init_GL();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les évènements de la fenêtre
  while (continuer) {
    // On attend le prochain évènement
    SDL_WaitEvent(&event);
    // On traite l'évènement
    switch (event.type) {
    case SDL_QUIT:
      // On a fermé la fenetre
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      // On a appuyé sur une touche
      continuer = keyboard(&event);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_RESIZED:
        reshape(event.window.data1, event.window.data2);
        break;
      }
      break;
    }
    display();
  }
  
  // C'est fini : on libère les ressources proprement
  SDL_SetWindowFullscreen(fenetre, 0);
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();
  
  return EXIT_SUCCESS;
}

/**
 * Initialisation de la SDL, création du contexte OpenGL et ouverture de la fenetre.
 */
void init_SDL(void) {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Erreur à l'initialisation de la vidéo : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  
  fenetre = SDL_CreateWindow("B-A-BA Text",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL\n");
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
}

/**
 * Initialisation de SDL_ttf,
 * on en profite pour charger la fonte qu'on utilisera plus tard.
 */
void init_TTF(void) {
  if (TTF_Init() == -1) {
    fprintf(stderr, "init_TTF(): impossible d'initialier la gestion  des polices (%s)\n", TTF_GetError());
    exit(EXIT_FAILURE);
  }
  
  font = TTF_OpenFont("LiberationSans-Regular.ttf", 32);
  if (font == NULL) {
    fprintf(stderr, "init_TTF(): impossible de charger la police (%s)\n", TTF_GetError());
    exit(EXIT_FAILURE);
  } 
}

/**
 * Une fonction permettant d'afficher du texte.
 * Pour aller plus loin, il faudrait éviter de récréer à chaque affichage
 * la texture contenant le texte si celui-ci n'a pas changé.
 * De même on pourrait avoir une option permettant de centrer le texte
 * autour de la position fournie en paramètre.
 * @param text la chaîne à afficher
 * @param x l'abscisse (en pixels) du coin supérieur gauche du texte à afficher
 * @param y l'ordonnée (en pixels) du coin supérieur gauche du texte à afficher.
 *          L'origine de l'axe des ordonnées est situé en haut de la fenêtre
 *          et il est orienté vers le bas.
 * @param color la couleur du texte à afficher.
 *              Attention les composantes sont comprises entre 0 et 255.
 */
void print_TTF(const char* text, GLuint x, GLuint y, SDL_Color color) {
  // On sauvegarde les attributs de la machine OpenGL
  // qui seront modifiés afin de pouvoir les restaurer
  // à la fin de l'affichage
  glPushAttrib(GL_ENABLE_BIT);
  glPushAttrib(GL_COLOR_BUFFER_BIT);
  glPushAttrib(GL_TEXTURE_BIT);

  // On en profite pour modifier la projection afin
  // que les coordonnées dans le plan (Oxy) de notre
  // monde virtuel correspondent à celles de la fenêtre.
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  GLint viewport[4];
  glGetIntegerv(GL_VIEWPORT, viewport);
  glLoadIdentity();
  glOrtho(0, viewport[2], viewport[3], 0, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();


  // On crée un image qui contiendra notre texte
  SDL_Surface *initial;

  initial = TTF_RenderUTF8_Blended(font, text, color);
  if (initial == NULL) {
    fprintf(stderr,
            "print_TTF(): Impossible de créer l'image pour le texte (%s)\n",
            TTF_GetError());
    exit(EXIT_FAILURE);
  }
  int w = initial->w;
  int h = initial->h;

  // On transforme l'image en texture
  SDL_LockSurface(initial);

  GLuint texture;
  glGenTextures(1, &texture);
  if (texture <= 0) {
    fprintf(stderr, "print_TTF(): Impossible d'allouer la texture\n");
    exit(EXIT_FAILURE);
  }
  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA,
                GL_UNSIGNED_BYTE, initial->pixels);

  SDL_UnlockSurface(initial);
  SDL_FreeSurface(initial);

  // On affiche le texte par dessus tout ce qui a déjà été dessiné
  glDisable(GL_DEPTH_TEST);

  glDisable(GL_CULL_FACE);

  // Le fond (transparent) de la texture n'interfère
  // pas avec ce qui est déjà dessiné
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);

  // La couleur du texte sera celle de la texture
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  glEnable(GL_TEXTURE_2D);

  // On dessine un rectangle sur lequel on plaque le
  // texte à afficher à l'endroit demandé
  glTranslatef(x, y, 0);
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0); glVertex3f(0, 0, 0);
  glTexCoord2f(1, 0); glVertex3f(w, 0, 0);
  glTexCoord2f(1, 1); glVertex3f(w, h, 0);
  glTexCoord2f(0, 1); glVertex3f(0, h, 0);
  glEnd();

  // On rend la machine OpenGL dans l'état où on l'a trouvée
  glDeleteTextures(1, &texture);
  glPopAttrib();
  glPopAttrib();
  glPopAttrib();

  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  // On vérifie qu'il n'y a pas d'erreur OpenGL en attente
  // de traitement
  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "print_TTF(): erreur OpenGL (%s)\n", gluErrorString(erreur));
  }
}


/**
 * Initialisation des états du contexte OpenGL.
 */
void init_GL(void) {
  glClearColor(1., 1., 1., 1.);
}

/**
 * Affichage de la scène OpenGL.
 */
void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // On dessine un carré rouge dans le plan Oxy
  glColor3f(1.0, 0.0, 0.0);
  glBegin(GL_QUADS);
  glVertex3f(-.2, -.2, 0.);
  glVertex3f(.2, -.2, 0.);
  glVertex3f(.2, .2, 0.);
  glVertex3f(-.2, .2, 0.);
  glEnd();

  print_TTF("Bonjour", 10, 10, (SDL_Color) {.r = 0, .g = 0, .b = 255});
  SDL_GL_SwapWindow(fenetre);

  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans display : %s\n", gluErrorString(erreur));
  }

}

/**
 * Gestion du clavier.
 * @param event l'évènement SDL ayant déclenché l'appel à la fonction
 * (doit etre de type SDL_KEYDOWN).
 */
int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    case SDLK_RETURN:
      if (!fullscreen) {
        if (SDL_SetWindowFullscreen(fenetre, SDL_WINDOW_FULLSCREEN) == 0) {
          fullscreen = true;
        } else {
          fprintf(stderr, "Erreur lors du passage au plein au écran : %s\n",
                  SDL_GetError());
        }
      } else {
        if (SDL_SetWindowFullscreen(fenetre, 0) == 0) {
          fullscreen = false;
        } else {
          fprintf(stderr, "Erreur lors du retour au mode fenêtré : %s\n",
                  SDL_GetError());          
        }
      }
      break;
    default:
      break;
    }
  }
  
  return 1;
}

/**
 * Modification de la taille de la fenêtre.
 */
void reshape(int w, int h) {
  glViewport(0, 0, w, h);

  // Il faudrait aussi redéfinir la projection...
}
