/**
 * Petit programme d'exemple de fonctionnement de la SDL avec OpenGL.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <SDL.h>
#include <GL/glu.h>
#include <GL/gl.h>

SDL_Window *fenetre;
SDL_GLContext context;

void init_SDL(void);
void init_GL(void);
void display(void);
int keyboard(SDL_Event * event);
void reshape(int w, int h);

bool fullscreen = false;

int main(int argc, char **argv) {
  if (argc != 1) {
    fprintf(stderr, "Usage: %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  init_SDL();
  init_GL();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les évènements de la fenêtre
  while (continuer) {
    // On attend le prochain évènement
    SDL_WaitEvent(&event);
    // On traite l'évènement
    switch (event.type) {
    case SDL_QUIT:
      // On a fermé la fenetre
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      // On a appuyé sur une touche
      continuer = keyboard(&event);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_RESIZED:
        reshape(event.window.data1, event.window.data2);
        break;
      }
      break;
    }
    display();
  }
  
  // C'est fini : on libère les ressources proprement
  SDL_SetWindowFullscreen(fenetre, 0);
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();
  
  return EXIT_SUCCESS;
}

/**
 * Initialisation de la SDL, création du contexte OpenGL et ouverture de la fenetre.
 */
void init_SDL(void) {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Erreur à l'initialisation de la vidéo : %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  
  fenetre = SDL_CreateWindow("SDL B-A-BA",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL\n");
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
}

/**
 * Initialisation des états du contexte OpenGL.
 */
void init_GL(void) {
  glClearColor(1., 1., 1., 0.);
}

/**
 * Affichage de la scène OpenGL.
 */
void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // On dessine un carré rouge dans le plan Oxy
  glColor3f(1.0, 0.0, 0.0);
  glBegin(GL_QUADS);
  glVertex3f(-.2, -.2, 0.);
  glVertex3f(.2, -.2, 0.);
  glVertex3f(.2, .2, 0.);
  glVertex3f(-.2, .2, 0.);
  glEnd();

  SDL_GL_SwapWindow(fenetre);

  GLenum erreur;
  if ((erreur = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans display : %s\n", gluErrorString(erreur));
  }

}

/**
 * Gestion du clavier.
 * @param event l'évènement SDL ayant déclenché l'appel à la fonction
 * (doit etre de type SDL_KEYDOWN).
 */
int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    case SDLK_RETURN:
      if (!fullscreen) {
        if (SDL_SetWindowFullscreen(fenetre, SDL_WINDOW_FULLSCREEN) == 0) {
          fullscreen = true;
        } else {
          fprintf(stderr, "Erreur lors du passage au plein au écran : %s\n",
                  SDL_GetError());
        }
      } else {
        if (SDL_SetWindowFullscreen(fenetre, 0) == 0) {
          fullscreen = false;
        } else {
          fprintf(stderr, "Erreur lors du retour au mode fenêtré : %s\n",
                  SDL_GetError());          
        }
      }
      break;
    default:
      break;
    }
  }
  
  return 1;
}

/**
 * Modification de la taille de la fenêtre.
 */
void reshape(int w, int h) {
  glViewport(0, 0, w, h);

  // Il faudrait aussi redéfinir la projection...
}
