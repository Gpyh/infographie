/**
 * Exemple de réflexion d'une scène sur un plan (ici le sol)
 * Version de << base >> :
 * - le rendu n'est pas correct si on regarde sous le sol ;
 * - la réflexion des objets dépasse du sol.
 */

#include <stdio.h>
#include <math.h>

#include <SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#ifndef true
#define true -1
#define false 0
#endif

#ifndef M_PI
#define M_PI 3.141592654
#endif

#define degRad(a) (((double) a) * M_PI / 180.)

SDL_Window *fenetre;
SDL_GLContext context;

/* Position de l'observateur */
float rho = 15;
float theta = 90; // angle (Ox, OP') (P' est le projeté de P dans le plan Oxz)
float phi = 30;   // angle (OP', OP)

/* Gestion de la souris */
int boutonGaucheAppuye = false;

/* Lumière (fixe par rapport à la scène) */
GLfloat lumPosition[4] = {3., 3., 0., 1.};

/* Image de la position de la source lumineuse selon une symétrie
 * par rapport au sol */
GLfloat lumPositionReflexion[4] = {3., -3., 0., 1.};

GLfloat lumiere[4] = {1., 1., 1., 1.};
GLfloat lumZero[4] = {0., 0., 0., 1.};
GLfloat matGrisAlpha[4] = {.4, .4, .4, .5};
GLfloat matRouge[4] = {.2, .0, .0, 1.};
GLfloat matVert[4] = {.0, .2, .0, 1.};
GLfloat matBleu[4] = {.0, .0, .2, 1.};
GLfloat matSpeculaire[4] = {1., 1., 1., 1.};
GLfloat matSpeculaireAlpha[4] = {1., 1., 1., .5};
GLfloat matShininess = 50.;

/* Une quadrique pour la GLU */
GLUquadric * quad;

GLenum erreur;

void positionneObservateur(void);
void reshape(int w, int h);
void init_SDL(void);
void init_GL(void);
void GL_Quit(void);
void dessineScene(void);
void dessineSol(void);
void display(void);
void motion(SDL_MouseMotionEvent motion);
void mouseButton(SDL_MouseButtonEvent button);
int keyboard(SDL_Event * event);

int main(int argc, char **argv) {
  if (argc != 1) {
    fprintf(stderr, "Usage: %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  init_SDL();
  init_GL();

  int continuer = 1;
  SDL_Event event;
  
  // Boucle traitant les évènements de la fenetre
  while (continuer) {
    SDL_WaitEvent(&event);
    switch (event.type) {
    case SDL_QUIT:
      continuer = 0;
      break;
    case SDL_KEYDOWN:
      continuer = keyboard(&event);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event) {
      case SDL_WINDOWEVENT_RESIZED:
        reshape(event.window.data1, event.window.data2);
        break;
      }
      break;
    case (SDL_MOUSEMOTION):
      {
        SDL_Event event_temp;
        while (SDL_PeepEvents(&event_temp, 1, SDL_PEEKEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION) > 0)
          SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_MOUSEMOTION, SDL_MOUSEMOTION);
      }
      motion(event.motion);
      break;
    case (SDL_MOUSEBUTTONDOWN) :
    case (SDL_MOUSEBUTTONUP) :
      mouseButton(event.button);
      break;
    }
    display();
  }
  
  GL_Quit();
  SDL_Quit();
  
  return EXIT_SUCCESS;
}

/**
 * Réalise un gluLookAt en fonction de la postion
 * (rho, theta, phi) de l'observateur.
 */
void positionneObservateur(void) {
  double posObsx, posObsy, posObsz;
  double basHautx, basHauty, basHautz;

  /* Calcul de la nouvelle position de l'observateur */
  posObsx = rho * cos(degRad(phi)) * cos(degRad(theta));
  posObsy = rho * sin(degRad(phi));
  posObsz = rho * cos(degRad(phi)) * sin(degRad(theta));

  /* Calcul du nouveau vecteur bas-haut */
  basHautx = sin(degRad(-phi)) * cos(degRad(theta));
  basHauty = cos(degRad(-phi));
  basHautz = sin(degRad(-phi)) * sin(degRad(theta));

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(posObsx, posObsy, posObsz, 0, 0, 0, basHautx, basHauty, basHautz);
}

void reshape(int w, int h) {
  glViewport(0, 0, w, h);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, (GLdouble) w / h, 8.5, 50);    
  glMatrixMode(GL_MODELVIEW);
}

void init_SDL(void) {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Couldn't initialize video driver: %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
  
  fenetre = SDL_CreateWindow("Réflexion",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

void init_GL(void) {
  glEnable(GL_DEPTH_TEST);
  
  glClearColor(0., 0., 0., 0.);

  glEnable(GL_LIGHTING);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lumZero);
  
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_AMBIENT, lumiere);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lumiere);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lumiere);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30, 1, 8.5, 50);
  glMatrixMode(GL_MODELVIEW);

  positionneObservateur();
  
  quad = gluNewQuadric();
}

void GL_Quit(void) {
  gluDeleteQuadric(quad);
}

/**
 * Affichage de la scene 
 */
void dessineScene(void) {
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpeculaire);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, matShininess);

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matRouge);
  glTranslatef(1., .5, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matVert);
  glTranslatef(-1., .5, 1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

  glPushMatrix();
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matBleu);
  glTranslatef(0., .5, -1.);
  gluSphere(quad, .5, 30, 30);
  glPopMatrix();

}

void dessineSol(void) {
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matGrisAlpha);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpeculaireAlpha);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, matShininess);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glNormal3f(0, 1, 0);
  float pas = 1;
  for (float i = -3; i < 3; i += pas) {
    glBegin(GL_QUAD_STRIP);
    for (float j = -3; j <= 3; j += pas) {
      glVertex3f(j, 0, i);
      glVertex3f(j, 0, i + pas);
    }
    glEnd();
  }
  glDisable(GL_BLEND);

}

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // On dessine la scène réfléchie
 
  // On change la position de la lumière pour tenir compte de
  // la réflexion
  glLightfv(GL_LIGHT0, GL_POSITION, lumPositionReflexion);

  glPushMatrix();
  // On réalise une symétrie par rapport au plan du sol
  // Ici on peut le faire à l'aide d'un glScale
  glScalef(1, -1, 1);

  // Il faut juste penser que dans ce cas l'orientation des faces est modifiée.
  glFrontFace(GL_CW);

  dessineScene();

  // On remet l'état de la machine OpenGL tel qu'il était
  glFrontFace(GL_CCW);
  glPopMatrix();
  glLightfv(GL_LIGHT0, GL_POSITION, lumPosition);

  /* Sol */
  dessineSol();
  glDisable(GL_BLEND);

  dessineScene();
  
  SDL_GL_SwapWindow(fenetre);

  if ((erreur = glGetError()) != GL_NO_ERROR) {
    printf("Erreur à la fin de l'affichage\n");
  }
}

/**
 * Fonction appelée lorsqu'on bouge la souris.
 */
void motion(SDL_MouseMotionEvent motion) {
  if (boutonGaucheAppuye) {
    theta += motion.xrel;
    phi += motion.yrel;
    positionneObservateur();
  }
}

/**
 * Fonction appelée lorsqu'on appuie sur un bouton de la souris.
 */
void mouseButton(SDL_MouseButtonEvent button) {
  if (button.button == SDL_BUTTON_LEFT) {
    if (button.state == SDL_PRESSED) {
      // On va déplacer l'observateur
      boutonGaucheAppuye = true;
    } else if (button.state == SDL_RELEASED) {
      boutonGaucheAppuye = false;
    }
  }
}

int keyboard(SDL_Event * event) {
  if (event->type == SDL_KEYDOWN) {
    switch(event->key.keysym.sym) {
    case SDLK_ESCAPE:
      return 0;
    default:
      break;
    }
  }
  
  return 1;
}
