#include <SDL.h>
#include <SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>

SDL_Window *fenetre;
SDL_GLContext context;

void initGL() {
  glClearColor(0.,0.,0.,1.0);
  glEnable(GL_DEPTH_TEST);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(-1, 1, -1, 1, 10, 100);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(0, 0, 20, 0, 0, 0, 0, 1, 0);
}

void initTexture() {
    glEnable(GL_TEXTURE_2D);

    // On crée un objet texture
    // Normalement, il faudrait détruire cet objet lorsqu'on n'en aura plus besoin
    // (ici il sera automatiquement détruit lorsque le contexte OpenGL sera détruit)
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    // On fixe les paramètres de la texture,
    // i.e. comment on calcule les coordonnées pixels -> texels.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // On charge l'image qui va nous servir de texture via la SDL
    SDL_Surface *image;
    image = IMG_Load("marblewhite.bmp");
    if (!image) {
      fprintf(stderr, "Impossible de charger l'image : %s\n", IMG_GetError());
      exit(EXIT_FAILURE);
    }

    // On verrouille l'image pendant qu'on l'utilise
    if (SDL_MUSTLOCK(image)) {
        SDL_LockSurface(image);
    }

    // On envoie le tableau contenant les texels de la texture à OpenGL
    if (image->format->BytesPerPixel == 3) {
      // L'image d'origine n'a que trois composantes
      if (image->format->Bmask == 255) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_BGR, GL_UNSIGNED_BYTE, image->pixels);
      } else {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
      }
    } else if (image->format->BytesPerPixel == 4) {
      // L'image d'origine à quatre composantes
      // (la dernière est le paramètre alpha qui va nous servir à gérer la transparence)
      if (image->format->Bmask == 255) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, image->pixels);
      } else {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
      }
    } else {
      fprintf(stderr, "Nombre de composants de l'image différent de 3 et 4\n");
      exit(EXIT_FAILURE);
    }

    // On libère les ressources occupées par l'image
    if (SDL_MUSTLOCK(image)) {
        SDL_UnlockSurface(image);
    }
    SDL_FreeSurface(image);

    // On spécifie comment la texture sera plaquée sur la facette
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    GLenum erreur;
    if ((erreur = glGetError()) != GL_NO_ERROR) {
        fprintf(stderr, "Erreur OpenGL dans setTexture : %s\n", gluErrorString(erreur));
    }

}

void display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  glRotatef(1, 1, -1, 0);

  glColor3f(1., 0., 0.);
  glBegin(GL_QUADS);
  // Il faut, en plus des coordonnées du sommet dans le monde virtuel,
  // spécifier les coordonnées du sommet dans la texture.
  glTexCoord2f(0., 0.);
  glVertex3f(-1, -1, 1);
  glTexCoord2f(1., 0.);
  glVertex3f( 1, -1, 1);
  glTexCoord2f(1., 1.);
  glVertex3f( 1,  1, 1);
  glTexCoord2f(0., 1.);
  glVertex3f(-1,  1, 1);
  glEnd();

  SDL_GL_SwapWindow(fenetre);

  if (glGetError() != GL_NO_ERROR) {
    fprintf(stderr, "Erreur OpenGL dans display\n");
  }
}

void init_SDL() {
  if (SDL_VideoInit(NULL) < 0) {
    fprintf(stderr, "Couldn't initialize video driver: %s\n",
            SDL_GetError());
    exit(EXIT_FAILURE);
  }
  
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    
  fenetre = SDL_CreateWindow("B-A-BA textures",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             640, 640,
                             SDL_WINDOW_OPENGL);
  
  if (fenetre == 0) {
    fprintf(stderr, "Erreur lors de la création de la fenêtre (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
  
  context = SDL_GL_CreateContext(fenetre);
  if (context == 0) {
    fprintf(stderr, "Erreur lors de la création du contexte OpenGL (SDL)\n");
    SDL_Quit();
    exit(EXIT_FAILURE);
  }
}

int main(void) {
  init_SDL();
  initGL();
  initTexture();

  int continuer = 1;
  SDL_Event event;
  while (continuer) {
      SDL_PollEvent(&event);
      switch(event.type) {
      case SDL_QUIT:
        continuer = 0;
        break;
      case SDL_KEYDOWN:
        switch(event.key.keysym.sym) {
        case SDLK_ESCAPE:
          continuer = 0;
          break;
        default:
          break;
        }
      default:
        break;
      }
      display();
  }
  
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();

  return 0;
}

